import type { ProColumns } from '@ant-design/pro-table';
import type { DictValueItem } from '@/services/system/types';
import { getDictValues } from '@/services/system/dictValue';
import { Tag } from 'antd';

export const DictValueColumns: ProColumns<DictValueItem>[] = [
  {
    title: '主键',
    dataIndex: 'id',
    hideInTable: true,
  },
  {
    title: '#',
    hideInSearch: true,
    width: 60,
    align: 'center',
    editable: false,
    render(_, __, index, action) {
      const current = action?.pageInfo?.current || 1;
      const pageSize = action?.pageInfo?.pageSize || 10;
      return (current - 1) * pageSize + index + 1;
    },
  },
  {
    title: '标签',
    dataIndex: 'name',
    render(dom, entity) {
      return <Tag color={entity.color}>{entity.name}</Tag>;
    },
  },
  {
    title: '取值',
    dataIndex: 'value',
    width: 200,
    ellipsis: true,
  },
  {
    title: '颜色',
    dataIndex: 'color',
    hideInSearch: true,
    valueType: 'color',
    fieldProps: { allowClear: true },
    align: 'center',
    width: 80,
  },
  {
    title: '默认值',
    dataIndex: 'defaultValue',
    valueType: 'switch',
    align: 'center',
    width: 80,
    render(dom, entity) {
      return entity.defaultValue ? <Tag color="success">是</Tag> : <Tag color="error">否</Tag>;
    },
  },
  {
    title: '状态',
    dataIndex: 'status',
    align: 'center',
    width: 120,
    valueType: 'select',
    valueEnum: await getDictValues('disable.enable', true),
  },
  {
    title: '排序',
    dataIndex: 'sort',
    valueType: 'digit',
    align: 'center',
    width: 100,
    initialValue: 1,
  },
  {
    title: '备注',
    dataIndex: 'remark',
    ellipsis: true,
  },
];
