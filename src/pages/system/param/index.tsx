import {
  PlusOutlined,
  DeleteOutlined,
  CloudDownloadOutlined,
  CloudUploadOutlined,
} from '@ant-design/icons';
import { Button, Popconfirm, message } from 'antd';
import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import { pageParam, addParam, updateParam, removeParam } from '@/services/system/param';
import type { ParamItem, ParamParams } from '@/services/system/types';
import { ProFormText, ProFormTextArea } from '@ant-design/pro-form';
import EditForm from '@/components/EditForm';
import SzhomeTable from '@/components/Custom/SzhomeTable';
/**
 * 添加系统参数
 *
 * @param fields
 */
const handleAdd = async (fields: ParamItem) => {
  const hide = message.loading('正在新增系统参数');

  try {
    await addParam({ ...fields });
    hide();
    message.success('系统参数添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('系统参数添加失败，请重试！');
    return false;
  }
};

/**
 * 更新系统参数
 *
 * @param fields
 */
const handleUpdate = async (fields: ParamItem, currentRow?: ParamItem) => {
  const hide = message.loading('正在更新系统参数');

  try {
    await updateParam({
      ...currentRow,
      ...fields,
    });
    hide();
    message.success('系统参数更新成功');
    return true;
  } catch (error) {
    hide();
    message.error('系统参数更新失败，请重试');
    return false;
  }
};

/**
 * 删除系统参数
 *
 * @param selectedRows
 */
const handleRemove = async (selectedRows: ParamItem[]) => {
  const hide = message.loading('正在删除系统参数');
  if (!selectedRows) return true;

  try {
    await removeParam(selectedRows.map((row) => row.id));
    hide();
    message.success('系统参数删除成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('系统参数删除失败，请重试');
    return false;
  }
};

const Param: React.FC = () => {
  /** 新建窗口的弹窗 */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<ParamItem>();
  const [selectedRowsState, setSelectedRows] = useState<ParamItem[]>([]);

  /** 国际化配置 */

  const columns: ProColumns<ParamItem>[] = [
    {
      title: '主键',
      dataIndex: 'id',
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: '#',
      hideInSearch: true,
      width: 60,
      align: 'center',
      render(_, __, index, action) {
        const current = action?.pageInfo?.current || 1;
        const pageSize = action?.pageInfo?.pageSize || 10;
        return (current - 1) * pageSize + index + 1;
      },
    },
    {
      title: '名称',
      dataIndex: 'name',
    },
    {
      title: '编码',
      dataIndex: 'code',
    },
    {
      title: '取值',
      dataIndex: 'value',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '创建人',
      dataIndex: 'createUser',
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: '修改人',
      dataIndex: 'updateUser',
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: '修改时间',
      dataIndex: 'updateTime',
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: '是否已删除',
      dataIndex: 'deleted',
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: '操作',
      align: 'center',
      dataIndex: 'option',
      valueType: 'option',
      width: 220,
      render: (_, record) => [
        <a
          key={'edit-' + record.id}
          onClick={() => {
            setCurrentRow(record);
            handleModalVisible(true);
          }}
        >
          编辑
        </a>,
        <Popconfirm
          key={'delete-' + record.id}
          title="您确定要删除此系统参数吗？"
          onConfirm={async () => {
            await handleRemove([record]);
            setSelectedRows([]);
            actionRef.current?.reloadAndRest?.();
          }}
          okText="Yes"
          cancelText="No"
        >
          <a key="remove">删除</a>
        </Popconfirm>,
      ],
    },
  ];

  return (
    <PageContainer>
      <SzhomeTable<ParamItem, ParamParams>
        actionRef={actionRef}
        request={pageParam}
        columns={columns}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              setCurrentRow(undefined);
              handleModalVisible(true);
            }}
          >
            <PlusOutlined /> 新增
          </Button>,

          <Popconfirm
            key={'batch_remove'}
            disabled={selectedRowsState?.length == 0}
            title={'您确定要删除已选' + selectedRowsState?.length + '条数据吗？'}
            onConfirm={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
            okText="Yes"
            cancelText="No"
          >
            <Button key="batch_remove" danger disabled={selectedRowsState?.length == 0}>
              <DeleteOutlined />
              删除
            </Button>
          </Popconfirm>,

          <Button
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <CloudDownloadOutlined /> 导出
          </Button>,

          <Button
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <CloudUploadOutlined /> 导入
          </Button>,
        ]}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
        }}
      />

      <EditForm
        visible={createModalVisible}
        name="系统参数"
        model={currentRow || { id: undefined }}
        onAdd={handleAdd}
        onUpdate={handleUpdate}
        afterEdit={() => {
          setCurrentRow(undefined);

          if (actionRef.current) {
            actionRef.current.reload();
          }
        }}
        onCancel={handleModalVisible}
      >
        <ProFormText name="id" hidden={true} />
        <ProFormText label="名称" name="name" required colProps={{ span: 12 }} />
        <ProFormText
          label="编码"
          name="code"
          required
          colProps={{ span: 12 }}
          disabled={!!currentRow?.id}
        />
        <ProFormTextArea label="取值" name="value" required />
        <ProFormTextArea label="备注" name="remark" />
      </EditForm>
    </PageContainer>
  );
};

export default Param;
