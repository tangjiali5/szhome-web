import type { ProColumns } from '@ant-design/pro-table';
import type { PostItem } from '@/services/system/types';
import { getDictValues } from '@/services/system/dictValue';

export const PostColumns: ProColumns<PostItem>[] = [
  {
    title: '主键',
    dataIndex: 'id',
    hideInSearch: true,
    hideInTable: true,
  },
  {
    title: '#',
    hideInSearch: true,
    width: 60,
    align: 'center',
    render(_, __, index, action) {
      const current = action?.pageInfo?.current || 1;
      const pageSize = action?.pageInfo?.pageSize || 10;
      return (current - 1) * pageSize + index + 1;
    },
  },
  {
    title: '名称',
    dataIndex: 'name',
  },
  {
    title: '编码',
    dataIndex: 'code',
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
  {
    title: '状态',
    dataIndex: 'status',
    valueType: 'select',
    valueEnum: await getDictValues('disable.enable'),
  },
];
