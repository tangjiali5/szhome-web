import {
  PlusOutlined,
  DeleteOutlined,
  CloudDownloadOutlined,
  CloudUploadOutlined,
  CopyOutlined,
  CheckCircleOutlined,
} from '@ant-design/icons';
import { Button, Card, Col, Popconfirm, Row, Tag, Tree, message } from 'antd';
import type { Key } from 'react';
import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import { pageRole, addRole, updateRole, removeRole } from '@/services/system/role';
import type { RoleItem, RoleParams } from '@/services/system/types';
import { ProFormText, ProFormTextArea, ProFormDigit, ModalForm } from '@ant-design/pro-form';
import EditForm from '@/components/EditForm';
import { getMenusOfRole, treeMenu } from '@/services/system/menu';
import { grantMenus } from '@/services/system/role';
import SzhomeTable from '@/components/Custom/SzhomeTable';

/**
 * 添加角色
 *
 * @param fields
 */
const handleAdd = async (fields: RoleItem) => {
  const hide = message.loading('正在新增角色');

  try {
    await addRole({ ...fields });
    hide();
    message.success('角色添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('角色添加失败，请重试！');
    return false;
  }
};

/**
 * 更新角色
 *
 * @param fields
 */
const handleUpdate = async (fields: RoleItem, currentRow?: RoleItem) => {
  const hide = message.loading('正在更新角色');

  try {
    await updateRole({
      ...currentRow,
      ...fields,
    });
    hide();
    message.success('角色更新成功');
    return true;
  } catch (error) {
    hide();
    message.error('角色更新失败，请重试');
    return false;
  }
};

/**
 * 删除角色
 *
 * @param selectedRows
 */
const handleRemove = async (selectedRows: RoleItem[]) => {
  const hide = message.loading('正在删除角色');
  if (!selectedRows) return true;

  try {
    await removeRole(selectedRows.map((row) => row.id));
    hide();
    message.success('角色删除成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('角色删除失败，请重试');
    return false;
  }
};

const menus = await treeMenu({});

const Role: React.FC = () => {
  /** 新建窗口的弹窗 */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<RoleItem>();
  const [selectedRoles, setSelectedRoles] = useState<RoleItem[]>([]);
  const [authVisible, handleAuthVisible] = useState<boolean>(false);
  const [checkedMenus, setCheckedMenus] = useState<Key[] | { checked: Key[]; halfChecked: Key[] }>(
    [],
  );
  const [selectedRows, setSelectedRows] = useState<Key[]>([]);

  /** 国际化配置 */

  const columns: ProColumns<RoleItem>[] = [
    {
      title: '主键',
      dataIndex: 'id',
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: '#',
      hideInSearch: true,
      width: 60,
      align: 'center',
      render(_, __, index, action) {
        const current = action?.pageInfo?.current || 1;
        const pageSize = action?.pageInfo?.pageSize || 10;
        return (current - 1) * pageSize + index + 1;
      },
    },
    {
      title: '名称',
      dataIndex: 'name',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '操作',
      align: 'center',
      dataIndex: 'option',
      valueType: 'option',
      width: 220,
      render: (_, record) => [
        <a
          key={'edit-' + record.id}
          onClick={() => {
            setCurrentRow(record);
            setSelectedRoles([record]);
            handleModalVisible(true);
          }}
        >
          编辑
        </a>,
        <a
          key={'auth-' + record.id}
          onClick={async () => {
            getMenusOfRole(record.id).then((res) => {
              handleAuthVisible(true);
              setCheckedMenus(res.data?.map((m) => m.id));
              setSelectedRoles([record]);
            });
          }}
        >
          授权
        </a>,
        <Popconfirm
          key={'delete-' + record.id}
          title="您确定要删除此角色吗？"
          onConfirm={async () => {
            await handleRemove([record]);
            setSelectedRoles([]);
            actionRef.current?.reloadAndRest?.();
          }}
          okText="Yes"
          cancelText="No"
        >
          <a key="remove">删除</a>
        </Popconfirm>,
      ],
    },
  ];

  return (
    <PageContainer>
      <SzhomeTable<RoleItem, RoleParams>
        actionRef={actionRef}
        request={pageRole}
        columns={columns}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              setCurrentRow(undefined);
              handleModalVisible(true);
            }}
          >
            <PlusOutlined /> 新增
          </Button>,

          <Button
            type="primary"
            key="primary"
            disabled={selectedRoles?.length == 0}
            onClick={() => {
              handleAuthVisible(true);
            }}
          >
            <CheckCircleOutlined /> 授权
          </Button>,

          <Popconfirm
            key={'batch_remove'}
            disabled={selectedRoles?.length == 0}
            title={'您确定要删除已选' + selectedRoles?.length + '条数据吗？'}
            onConfirm={async () => {
              await handleRemove(selectedRoles);
              setSelectedRoles([]);
              actionRef.current?.reloadAndRest?.();
            }}
            okText="Yes"
            cancelText="No"
          >
            <Button key="batch_remove" danger disabled={selectedRoles?.length == 0}>
              <DeleteOutlined />
              删除
            </Button>
          </Popconfirm>,

          <Button
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <CloudDownloadOutlined /> 导出
          </Button>,

          <Button
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <CloudUploadOutlined /> 导入
          </Button>,
        ]}
        rowSelection={{
          selectedRowKeys: selectedRows,
          onChange: (_, roles) => {
            setSelectedRoles(roles);
            setSelectedRows(roles.map((r) => r.id));
          },
        }}
      />

      <EditForm
        visible={createModalVisible}
        name="角色"
        model={currentRow || { id: undefined }}
        onAdd={handleAdd}
        onUpdate={handleUpdate}
        afterEdit={() => {
          setCurrentRow(undefined);

          if (actionRef.current) {
            actionRef.current.reload();
          }
        }}
        onCancel={handleModalVisible}
      >
        <ProFormText name="id" hidden={true} />
        <ProFormText label="名称" name="name" required colProps={{ span: 12 }} />
        <ProFormDigit
          label="顺序"
          name="sort"
          min={0}
          initialValue={0}
          fieldProps={{ precision: 0 }}
          colProps={{ span: 12 }}
        />
        <ProFormTextArea label="备注" name="remark" />
      </EditForm>

      <ModalForm
        title="角色授权"
        visible={authVisible}
        onVisibleChange={(visible) => {
          handleAuthVisible(visible);
          if (!visible) {
            setSelectedRoles([]);
            setCheckedMenus([]);
            setSelectedRows([]);
          }
        }}
        width={450}
        modalProps={{
          destroyOnClose: true,
        }}
        onFinish={async () => {
          const res = await grantMenus(
            selectedRoles.map((row) => row.id.toString()),
            checkedMenus,
          );
          if (res.success) {
            message.success('角色授权成功');
          } else {
            message.error('角色授权失败');
          }
          return res.success;
        }}
      >
        <Card
          title="已选角色"
          headStyle={{
            fontSize: 13,
            fontWeight: 'bold',
            height: 30,
          }}
          style={{
            marginBottom: 20,
          }}
        >
          <Row gutter={[10, 10]}>
            {selectedRoles.map((o) => {
              return (
                <Col span={8} key={o.id}>
                  <Tag key={o.id} color="processing" style={{ width: '100%', textAlign: 'center' }}>
                    {o.name}
                  </Tag>
                </Col>
              );
            })}
          </Row>
        </Card>
        <Card
          title="菜单列表"
          headStyle={{
            fontSize: 13,
            fontWeight: 'bold',
            height: 30,
          }}
          bodyStyle={{
            maxHeight: 300,
            overflow: 'scroll',
          }}
        >
          <Tree
            treeData={menus.data}
            checkable
            defaultExpandAll={true}
            showIcon={true}
            showLine={true}
            checkedKeys={checkedMenus}
            icon={<CopyOutlined />}
            onCheck={setCheckedMenus}
            fieldNames={{
              key: 'id',
              title: 'name',
            }}
          />
        </Card>
      </ModalForm>
    </PageContainer>
  );
};

export default Role;
