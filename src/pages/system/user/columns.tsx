import type { ProColumns } from '@ant-design/pro-table';
import type { UserItem } from '@/services/system/types';
import { getDictValues } from '@/services/system/dictValue';
import { treeDept } from '@/services/system/dept';
import { ManOutlined, QuestionCircleOutlined } from '@ant-design/icons';

export const UserColumns: ProColumns<UserItem>[] = [
  {
    title: '主键',
    dataIndex: 'id',
    hideInSearch: true,
    hideInTable: true,
  },
  {
    title: '#',
    hideInSearch: true,
    width: 60,
    align: 'center',
    render(_, __, index, action) {
      const current = action?.pageInfo?.current || 1;
      const pageSize = action?.pageInfo?.pageSize || 10;
      return (current - 1) * pageSize + index + 1;
    },
  },
  {
    title: '姓名',
    dataIndex: 'realName',
    width: 120,
    ellipsis: true,
  },
  {
    title: '账号',
    dataIndex: 'username',
    width: 120,
    copyable: true,
    ellipsis: true,
  },
  {
    title: '密码',
    dataIndex: 'password',
    hideInSearch: true,
    hideInTable: true,
  },
  {
    title: '头像',
    dataIndex: 'avatar',
    hideInSearch: true,
    hideInTable: true,
  },
  {
    title: '手机',
    dataIndex: 'phone',
    width: 140,
    ellipsis: true,
    copyable: true,
  },
  {
    title: '邮箱',
    dataIndex: 'email',
    width: 220,
    ellipsis: true,
    copyable: true,
  },
  {
    title: '性别',
    dataIndex: 'gender',
    valueType: 'checkbox',
    valueEnum: await getDictValues('gender'),
    hideInTable: true,
  },
  {
    title: '性别',
    align: 'center',
    width: 60,
    dataIndex: 'gender',
    valueType: 'radio',
    valueEnum: await getDictValues('gender'),
    render(dom, entity) {
      if (entity.gender == '1') {
        return <ManOutlined />;
      }

      if (entity.gender == '2') {
        return <ManOutlined />;
      }

      return <QuestionCircleOutlined />;
    },
    hideInSearch: true,
  },
  {
    title: '生日',
    dataIndex: 'birthday',
    valueType: 'dateRange',
    hideInTable: true,
  },
  {
    title: '生日',
    align: 'center',
    width: 120,
    dataIndex: 'birthday',
    hideInSearch: true,
    renderText(text) {
      return text;
    },
  },
  {
    title: '部门',
    dataIndex: 'deptId',
    valueType: 'treeSelect',
    order: 999,
    ellipsis: true,
    fieldProps: {
      allowClear: true,
      fieldNames: {
        label: 'name',
        value: 'id',
      },
    },
    request: async () => {
      const res = await treeDept({});
      return res.data;
    },
    render(dom, entity) {
      return entity.deptId == 0 ? '-' : dom;
    },
  },
  {
    title: '状态',
    align: 'center',
    width: 100,
    ellipsis: true,
    dataIndex: 'status',
    valueType: 'select',
    valueEnum: await getDictValues('user.status'),
  },
];
