import type { ProColumns } from '@ant-design/pro-table';
import type { MenuItem } from '@/services/system/types';
import { getDictValues } from '@/services/system/dictValue';
import { CopyOutlined } from '@ant-design/icons';
import { treeMenu } from '@/services/system/menu';

export const MenuColumns: ProColumns<MenuItem>[] = [
  {
    title: '主键',
    dataIndex: 'id',
    hideInSearch: true,
    hideInTable: true,
  },
  {
    title: '上级菜单',
    dataIndex: 'parentId',
    valueType: 'treeSelect',
    hideInTable: true,
    fieldProps: {
      treeDefaultExpandAll: true,
      icon: <CopyOutlined />,
      treeLine: true,
      treeIcon: true,
      fieldNames: {
        label: 'name',
        value: 'id',
      },
    },
    request: async () => {
      const res = await treeMenu({});
      return res.data;
    },
  },
  {
    title: '名称',
    dataIndex: 'name',
  },
  {
    title: '编码',
    dataIndex: 'code',
  },
  {
    title: '请求路径',
    dataIndex: 'path',
  },
  {
    title: '权限标识',
    dataIndex: 'perms',
    hideInForm: true,
    hideInTable: true,
  },
  {
    title: '类型',
    dataIndex: 'type',
    valueType: 'select',
    valueEnum: await getDictValues('menu.type'),
    align: 'center',
  },
  {
    title: '是否隐藏',
    dataIndex: 'visible',
    valueType: 'select',
    valueEnum: await getDictValues('yes.no'),
    align: 'center',
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
  {
    title: '状态',
    dataIndex: 'status',
    valueType: 'select',
    valueEnum: await getDictValues('disable.enable'),
    align: 'center',
  },
];
