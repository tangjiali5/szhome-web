import {
  PlusOutlined,
  DeleteOutlined,
  CloudDownloadOutlined,
  CloudUploadOutlined,
} from '@ant-design/icons';
import { Button, Modal, Popconfirm, message } from 'antd';
import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import { pageDict, addDict, updateDict, removeDict } from '@/services/system/dict';
import type { DictItem, DictParams } from '@/services/system/types';
import { ProFormText, ProFormTextArea } from '@ant-design/pro-form';
import EditForm from '@/components/EditForm';
import DictValue from '@/pages/system/dictValue';
import SzhomeTable from '@/components/Custom/SzhomeTable';

/**
 * 添加数据字典
 *
 * @param fields
 */
const handleAdd = async (fields: DictItem) => {
  const hide = message.loading('正在新增数据字典');

  try {
    await addDict({ ...fields });
    hide();
    message.success('数据字典添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('数据字典添加失败，请重试！');
    return false;
  }
};

/**
 * 更新数据字典
 *
 * @param fields
 */
const handleUpdate = async (fields: DictItem, currentRow?: DictItem) => {
  const hide = message.loading('正在更新数据字典');

  try {
    await updateDict({
      ...currentRow,
      ...fields,
    });
    hide();
    message.success('数据字典更新成功');
    return true;
  } catch (error) {
    hide();
    message.error('数据字典更新失败，请重试');
    return false;
  }
};

/**
 * 删除数据字典
 *
 * @param selectedRows
 */
const handleRemove = async (selectedRows: DictItem[]) => {
  const hide = message.loading('正在删除数据字典');
  if (!selectedRows) return true;

  try {
    await removeDict(selectedRows.map((row) => row.id));
    hide();
    message.success('数据字典删除成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('数据字典删除失败，请重试');
    return false;
  }
};

const Dict: React.FC = () => {
  /** 新建窗口的弹窗 */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<DictItem>();
  const [selectedRowsState, setSelectedRows] = useState<DictItem[]>([]);
  const [dictValuesVisible, setDictValuesVisible] = useState<boolean>(false);

  /** 国际化配置 */

  const columns: ProColumns<DictItem>[] = [
    {
      title: '主键',
      dataIndex: 'id',
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: '名称',
      dataIndex: 'name',
    },
    {
      title: '编码',
      dataIndex: 'code',
    },
    {
      title: '备注',
      dataIndex: 'remark',
    },
    {
      title: '创建人',
      dataIndex: 'createUser',
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: '创建时间',
      dataIndex: 'createTime',
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: '修改人',
      dataIndex: 'updateUser',
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: '修改时间',
      dataIndex: 'updateTime',
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: '是否已删除',
      dataIndex: 'deleted',
      hideInSearch: true,
      hideInTable: true,
    },
    {
      title: '操作',
      align: 'center',
      dataIndex: 'option',
      valueType: 'option',
      width: 220,
      render: (_, record) => [
        <a
          key={'edit-' + record.id}
          onClick={() => {
            setCurrentRow(record);
            handleModalVisible(true);
          }}
        >
          编辑
        </a>,
        <a
          key={'items-' + record.id}
          onClick={() => {
            setCurrentRow(record);
            setDictValuesVisible(true);
          }}
        >
          字典项
        </a>,
        <Popconfirm
          key={'delete-' + record.id}
          title="您确定要删除此数据字典吗？"
          onConfirm={async () => {
            await handleRemove([record]);
            setSelectedRows([]);
            actionRef.current?.reloadAndRest?.();
          }}
          okText="Yes"
          cancelText="No"
        >
          <a key="remove">删除</a>
        </Popconfirm>,
      ],
    },
  ];

  return (
    <PageContainer>
      <SzhomeTable<DictItem, DictParams>
        actionRef={actionRef}
        request={pageDict}
        columns={columns}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              setCurrentRow(undefined);
              handleModalVisible(true);
            }}
          >
            <PlusOutlined /> 新增
          </Button>,

          <Popconfirm
            key={'batch_remove'}
            disabled={selectedRowsState?.length == 0}
            title={'您确定要删除已选' + selectedRowsState?.length + '条数据吗？'}
            onConfirm={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
            okText="Yes"
            cancelText="No"
          >
            <Button key="batch_remove" danger disabled={selectedRowsState?.length == 0}>
              <DeleteOutlined />
              删除
            </Button>
          </Popconfirm>,

          <Button
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <CloudDownloadOutlined /> 导出
          </Button>,

          <Button
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <CloudUploadOutlined /> 导入
          </Button>,
        ]}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
        }}
      />

      <EditForm
        visible={createModalVisible}
        name="数据字典"
        model={currentRow || { id: undefined }}
        onAdd={handleAdd}
        onUpdate={handleUpdate}
        afterEdit={() => {
          setCurrentRow(undefined);

          if (actionRef.current) {
            actionRef.current.reload();
          }
        }}
        onCancel={handleModalVisible}
      >
        <ProFormText name="id" hidden={true} />
        <ProFormText label="名称" required name="name" colProps={{ span: 12 }} />
        <ProFormText label="编码" required name="code" colProps={{ span: 12 }} />
        <ProFormTextArea label="备注" name="remark" />
      </EditForm>

      <Modal
        title={currentRow?.name + '【' + currentRow?.code + '】'}
        width={1200}
        open={dictValuesVisible}
        destroyOnClose={true}
        footer={null}
        onCancel={() => setDictValuesVisible(false)}
        bodyStyle={{
          padding: '0px 5px',
        }}
      >
        <DictValue dict={currentRow || { id: '-1' }} />
      </Modal>
      {/*
      <Drawer
        title="数据字典项"
        width={1200}
        open={dictValuesVisible}
        onClose={() => setDictValuesVisible(false)}
        destroyOnClose={true}
        bodyStyle={{
          padding: '0px',
        }}
      >
        <DictValue dict={currentRow || { id: '-1' }} />
      </Drawer> */}
    </PageContainer>
  );
};

export default Dict;
