import type { ProColumns } from '@ant-design/pro-table';
import type { DeptItem } from '@/services/system/types';
import { getDictValues } from '@/services/system/dictValue';
import { treeDept } from '@/services/system/dept';
import { listUser } from '@/services/system/user';

export const DeptColumns: ProColumns<DeptItem>[] = [
  {
    title: '主键',
    dataIndex: 'id',
    hideInSearch: true,
    hideInTable: true,
  },
  {
    title: '上级部门',
    dataIndex: 'parentId',
    hideInTable: true,
    valueType: 'treeSelect',
    fieldProps: {
      fieldNames: {
        label: 'name',
        value: 'id',
      },
    },
    request: async () => {
      const res = await treeDept({});
      return res.data;
    },
  },
  {
    title: '名称',
    dataIndex: 'name',
  },
  {
    title: '类型',
    dataIndex: 'type',
    valueType: 'select',
    valueEnum: await getDictValues('dept.type'),
  },
  {
    title: '负责人',
    dataIndex: 'ownerId',
    valueType: 'select',
    request: listUser,
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
  {
    title: '状态',
    dataIndex: 'status',
    valueType: 'select',
    valueEnum: await getDictValues('disable.enable'),
  },
];
