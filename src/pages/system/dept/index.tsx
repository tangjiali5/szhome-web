import {
  PlusOutlined,
  DeleteOutlined,
  CloudDownloadOutlined,
  CloudUploadOutlined,
} from '@ant-design/icons';
import { Button, Popconfirm, message } from 'antd';
import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import { addDept, updateDept, removeDept } from '@/services/system/dept';
import type { DeptItem, DeptParams } from '@/services/system/types';
import { ProFormText, ProFormTextArea, ProFormDigit } from '@ant-design/pro-form';
import EditForm from '@/components/EditForm';
import { treeDept } from '@/services/system/dept';
import DeptTree from '@/components/DeptTree';
import UserSelect from '@/components/UserSelect';
import DictSelect from '@/components/DictSelect';
import { DeptColumns } from './columns';
import SzhomeTable from '@/components/Custom/SzhomeTable';

/**
 * 添加部门
 *
 * @param fields
 */
const handleAdd = async (fields: DeptItem) => {
  const hide = message.loading('正在新增部门');

  try {
    await addDept({ ...fields });
    hide();
    message.success('部门添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('部门添加失败，请重试！');
    return false;
  }
};

/**
 * 更新部门
 *
 * @param fields
 */
const handleUpdate = async (fields: DeptItem, currentRow?: DeptItem) => {
  const hide = message.loading('正在更新部门');

  try {
    await updateDept({
      ...currentRow,
      ...fields,
    });
    hide();
    message.success('部门更新成功');
    return true;
  } catch (error) {
    hide();
    message.error('部门更新失败，请重试');
    return false;
  }
};

/**
 * 删除部门
 *
 * @param selectedRows
 */
const handleRemove = async (selectedRows: DeptItem[]) => {
  const hide = message.loading('正在删除部门');
  if (!selectedRows) return true;

  try {
    await removeDept(selectedRows.map((row) => row.id));
    hide();
    message.success('部门删除成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('部门删除失败，请重试');
    return false;
  }
};

const Dept: React.FC = () => {
  /** 新建窗口的弹窗 */
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<DeptItem>();
  const [selectedRowsState, setSelectedRows] = useState<DeptItem[]>([]);

  /** 国际化配置 */

  const columns: ProColumns<DeptItem>[] = [
    ...DeptColumns,
    {
      title: '操作',
      align: 'center',
      dataIndex: 'option',
      valueType: 'option',
      width: 220,
      render: (_, record) => [
        <a
          key={'edit-' + record.id}
          onClick={() => {
            setCurrentRow(record);
            handleModalVisible(true);
          }}
        >
          编辑
        </a>,
        <Popconfirm
          key={'delete-' + record.id}
          title="您确定要删除此部门吗？"
          onConfirm={async () => {
            await handleRemove([record]);
            setSelectedRows([]);
            actionRef.current?.reloadAndRest?.();
          }}
          okText="Yes"
          cancelText="No"
        >
          <a key="remove">删除</a>
        </Popconfirm>,
      ],
    },
  ];

  return (
    <PageContainer>
      <SzhomeTable<DeptItem, DeptParams>
        actionRef={actionRef}
        request={treeDept}
        columns={columns}
        pagination={false}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              setCurrentRow(undefined);
              handleModalVisible(true);
            }}
          >
            <PlusOutlined /> 新增
          </Button>,

          <Popconfirm
            key={'batch_remove'}
            disabled={selectedRowsState?.length == 0}
            title={'您确定要删除已选' + selectedRowsState?.length + '条数据吗？'}
            onConfirm={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
            okText="Yes"
            cancelText="No"
          >
            <Button key="batch_remove" danger disabled={selectedRowsState?.length == 0}>
              <DeleteOutlined />
              删除
            </Button>
          </Popconfirm>,

          <Button
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <CloudDownloadOutlined /> 导出
          </Button>,

          <Button
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <CloudUploadOutlined /> 导入
          </Button>,
        ]}
        rowSelection={{
          checkStrictly: false,
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
        }}
      />

      <EditForm
        visible={createModalVisible}
        name="部门"
        model={currentRow || { id: undefined }}
        onAdd={handleAdd}
        onUpdate={handleUpdate}
        afterEdit={() => {
          setCurrentRow(undefined);

          if (actionRef.current) {
            actionRef.current.reload();
          }
        }}
        onCancel={handleModalVisible}
      >
        <ProFormText name="id" hidden={true} />
        <DeptTree title="上级部门" name="parentId" required cols={12} />
        {/* <ProFormText label="负责人" required name="ownerId" colProps={{ span: 12 }} /> */}
        {/* <ProFormSelect
          label='负责人'
          name='ownerId'
          colProps={{span: 12}}
          required
          request={list}
        /> */}
        <UserSelect title="负责人" name="ownerId" required cols={12} />
        <ProFormText label="名称" required name="name" colProps={{ span: 12 }} />
        <DictSelect label="类型" name="type" dict="dept.type" cols={12} />
        <ProFormDigit
          required
          label="顺序"
          name="sort"
          min={0}
          initialValue={0}
          fieldProps={{ precision: 0 }}
          colProps={{ span: 12 }}
        />
        <DictSelect label="状态" name="status" dict="disable.enable" cols={12} />
        <ProFormTextArea label="备注" name="remark" />
      </EditForm>
    </PageContainer>
  );
};

export default Dept;
