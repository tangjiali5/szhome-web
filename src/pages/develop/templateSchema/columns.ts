import type { ProColumns } from '@ant-design/pro-table';
import type { TemplateSchemaItem } from '@/services/develop/types';

export const TemplateSchemaColumns: ProColumns<TemplateSchemaItem>[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    hideInSearch: true,
    hideInTable: true,
  },
  {
    title: '#',
    hideInSearch: true,
    width: 60,
    align: 'center',
    render(_, __, index, action) {
      const current = action?.pageInfo?.current || 1;
      const pageSize = action?.pageInfo?.pageSize || 10;
      return (current - 1) * pageSize + index + 1;
    },
  },
  {
    title: '方案名称',
    dataIndex: 'name',
    width: 300,
    ellipsis: true,
  },
  {
    title: '方案编码',
    dataIndex: 'code',
    width: 200,
    ellipsis: true,
  },
  {
    title: '备注',
    dataIndex: 'remark',
    ellipsis: true,
  },
];
