import {
  PlusOutlined,
  DeleteOutlined,
  CloudDownloadOutlined,
  CloudUploadOutlined,
} from '@ant-design/icons';
import { Button, Drawer, Popconfirm } from 'antd';
import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import type { TemplateSchemaItem, TemplateSchemaParams } from '@/services/develop/types';
import { ProFormText, ProFormTextArea } from '@ant-design/pro-form';
import EditForm from '@/components/EditForm';
import { TemplateSchemaColumns } from './columns';
import { handleAdd, handleUpdate, handleRemove } from './action';
import { pageTemplateSchema } from '@/services/develop/templateSchema';
import Template from './template';
import SzhomeTable from '@/components/Custom/SzhomeTable';

const TemplateSchema: React.FC = () => {
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<TemplateSchemaItem>();
  const [selectedRowsState, setSelectedRows] = useState<TemplateSchemaItem[]>([]);

  const [configTemplateVisible, setConfigTemplateVisible] = useState<boolean>(false);

  const columns: ProColumns<TemplateSchemaItem>[] = [
    ...TemplateSchemaColumns,
    {
      title: '操作',
      align: 'center',
      dataIndex: 'option',
      valueType: 'option',
      width: 220,
      render: (_, record) => [
        <a
          key={'edit-' + record.id}
          onClick={() => {
            setCurrentRow(record);
            handleModalVisible(true);
          }}
        >
          编辑
        </a>,
        <a
          key={'template-' + record.id}
          onClick={() => {
            setCurrentRow(record);
            setConfigTemplateVisible(true);
          }}
        >
          模板
        </a>,
        <Popconfirm
          key={'delete-' + record.id}
          title="您确定要删除此模板方案吗？"
          onConfirm={async () => {
            await handleRemove([record]);
            setSelectedRows([]);
            actionRef.current?.reloadAndRest?.();
          }}
          okText="Yes"
          cancelText="No"
        >
          <a key="remove">删除</a>
        </Popconfirm>,
      ],
    },
  ];

  return (
    <PageContainer>
      <SzhomeTable<TemplateSchemaItem, TemplateSchemaParams>
        actionRef={actionRef}
        rowKey="id"
        request={pageTemplateSchema}
        columns={columns}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              setCurrentRow(undefined);
              handleModalVisible(true);
            }}
          >
            <PlusOutlined /> 新增
          </Button>,
          <Popconfirm
            key={'batch_remove'}
            disabled={selectedRowsState?.length == 0}
            title={'您确定要删除已选' + selectedRowsState?.length + '条模板方案数据吗？'}
            onConfirm={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
            okText="Yes"
            cancelText="No"
          >
            <Button key="batch_remove" danger disabled={selectedRowsState?.length == 0}>
              <DeleteOutlined />
              删除
            </Button>
          </Popconfirm>,

          <Button
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <CloudDownloadOutlined /> 导出
          </Button>,

          <Button
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <CloudUploadOutlined /> 导入
          </Button>,
        ]}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
        }}
      />

      <EditForm
        visible={createModalVisible}
        name="模板方案"
        model={currentRow || { id: undefined }}
        onAdd={handleAdd}
        onUpdate={handleUpdate}
        afterEdit={() => {
          setCurrentRow(undefined);

          if (actionRef.current) {
            actionRef.current.reload();
          }
        }}
        onCancel={handleModalVisible}
      >
        <ProFormText name="id" hidden={true} />
        <ProFormText required label="方案名称" name="name" colProps={{ span: 12 }} />
        <ProFormText required label="方案编码" name="code" colProps={{ span: 12 }} />
        <ProFormTextArea label="备注" name="remark" />
      </EditForm>

      <Drawer
        width="1440"
        open={configTemplateVisible}
        onClose={() => setConfigTemplateVisible(false)}
        destroyOnClose={true}
        afterOpenChange={() => {}}
      >
        <Template schemaId={currentRow?.id || ''} />
      </Drawer>
    </PageContainer>
  );
};

export default TemplateSchema;
