import type { TemplateItem } from '@/services/develop/types';
import { addTemplate, updateTemplate, removeTemplate } from '@/services/develop/template';
import { message } from 'antd';

/**
 * 添加模版
 *
 * @param fields
 */
export const handleAdd = async (fields: TemplateItem) => {
  const hide = message.loading('正在新增模版');

  try {
    await addTemplate({ ...fields });
    hide();
    message.success('模版添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('模版添加失败，请重试！');
    return false;
  }
};

/**
 * 更新模版
 *
 * @param fields
 */
export const handleUpdate = async (fields: TemplateItem, currentRow?: TemplateItem) => {
  const hide = message.loading('正在更新模版');

  try {
    await updateTemplate({
      ...currentRow,
      ...fields,
    });
    hide();
    message.success('模版更新成功');
    return true;
  } catch (error) {
    hide();
    message.error('模版更新失败，请重试');
    return false;
  }
};

/**
 * 删除模版
 *
 * @param selectedRows
 */
export const handleRemove = async (selectedRows: TemplateItem[]) => {
  const hide = message.loading('正在删除模版');
  if (!selectedRows) return true;

  try {
    await removeTemplate(selectedRows.map((row) => row.id));
    hide();
    message.success('模版删除成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('模版删除失败，请重试');
    return false;
  }
};
