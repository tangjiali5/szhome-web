import type { ProColumns } from '@ant-design/pro-table';
import type { TemplateItem } from '@/services/develop/types';

export const TemplateColumns: ProColumns<TemplateItem>[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    hideInSearch: true,
    hideInTable: true,
  },
  {
    title: '#',
    hideInSearch: true,
    width: 60,
    align: 'center',
    render(_, __, index, action) {
      const current = action?.pageInfo?.current || 1;
      const pageSize = action?.pageInfo?.pageSize || 10;
      return (current - 1) * pageSize + index + 1;
    },
  },
  {
    title: '模板方案',
    dataIndex: 'templateSchemaId',
  },
  {
    title: '模板名称',
    dataIndex: 'name',
  },
  {
    title: '模板类型',
    dataIndex: 'type',
  },
  {
    title: '输出路径',
    dataIndex: 'path',
  },
  {
    title: '文件名',
    dataIndex: 'file',
  },
  {
    title: '模板内容',
    dataIndex: 'template',
  },
  {
    title: '模板引擎',
    dataIndex: 'engine',
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
];
