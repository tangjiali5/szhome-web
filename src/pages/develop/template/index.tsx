import {
  PlusOutlined,
  DeleteOutlined,
  CloudDownloadOutlined,
  CloudUploadOutlined,
} from '@ant-design/icons';
import { Button, Col, Popconfirm } from 'antd';
import React, { useState, useRef } from 'react';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import type { TemplateItem, TemplateParams } from '@/services/develop/types';
import { ProFormText } from '@ant-design/pro-form';
import EditForm from '@/components/EditForm';
import { TemplateColumns } from './columns';
import { handleAdd, handleUpdate, handleRemove } from './action';
import { pageTemplate } from '@/services/develop/template';

import CodeMirror from '@uiw/react-codemirror';
import { loadLanguage } from '@uiw/codemirror-extensions-langs';
import DictSelect from '@/components/DictSelect';
import SzhomeTable from '@/components/Custom/SzhomeTable';

const Template: React.FC = () => {
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<TemplateItem>();
  const [selectedRowsState, setSelectedRows] = useState<TemplateItem[]>([]);

  const columns: ProColumns<TemplateItem>[] = [
    ...TemplateColumns,
    {
      title: '操作',
      align: 'center',
      dataIndex: 'option',
      valueType: 'option',
      width: 220,
      render: (_, record) => [
        <a
          key={'edit-' + record.id}
          onClick={() => {
            setCurrentRow(record);
            handleModalVisible(true);
          }}
        >
          编辑
        </a>,
        <Popconfirm
          key={'delete-' + record.id}
          title="您确定要删除此模版吗？"
          onConfirm={async () => {
            await handleRemove([record]);
            setSelectedRows([]);
            actionRef.current?.reloadAndRest?.();
          }}
          okText="Yes"
          cancelText="No"
        >
          <a key="remove">删除</a>
        </Popconfirm>,
      ],
    },
  ];

  return (
    <>
      <SzhomeTable<TemplateItem, TemplateParams>
        actionRef={actionRef}
        request={pageTemplate}
        columns={columns}
        pagination={false}
        search={false}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              setCurrentRow(undefined);
              handleModalVisible(true);
            }}
          >
            <PlusOutlined /> 新增
          </Button>,
          <Popconfirm
            key={'batch_remove'}
            disabled={selectedRowsState?.length == 0}
            title={'您确定要删除已选' + selectedRowsState?.length + '条模版数据吗？'}
            onConfirm={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
            okText="Yes"
            cancelText="No"
          >
            <Button key="batch_remove" danger disabled={selectedRowsState?.length == 0}>
              <DeleteOutlined />
              删除
            </Button>
          </Popconfirm>,

          <Button
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <CloudDownloadOutlined /> 导出
          </Button>,

          <Button
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <CloudUploadOutlined /> 导入
          </Button>,
        ]}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
        }}
      />

      <EditForm
        visible={createModalVisible}
        name="模版"
        width={1440}
        model={currentRow || { id: undefined }}
        onAdd={handleAdd}
        onUpdate={handleUpdate}
        afterEdit={() => {
          setCurrentRow(undefined);

          if (actionRef.current) {
            actionRef.current.reload();
          }
        }}
        onCancel={handleModalVisible}
      >
        <ProFormText name="id" hidden={true} />
        {/* <ProFormText label="模板方案" name="templateSchemaId" colProps={{ xs: 8 }} /> */}
        <ProFormText required label="模板名称" name="name" colProps={{ xs: 8 }} />
        <DictSelect label="模板类型" name="type" cols={8} dict="language.programe" />
        <DictSelect label="模板引擎" name="engine" cols={8} dict="template.engine" />
        <ProFormText
          required
          label="输出路径"
          name="path"
          placeholder={'支持模板表达式，如${model.module?uncap_first}/service/impl'}
          colProps={{ xs: 16 }}
        />
        <ProFormText
          required
          label="文件名"
          name="file"
          placeholder={'支持模板表达式，如${model.name?cap_first}Service.java'}
          colProps={{ xs: 8 }}
        />
        {/* <ProFormTextArea label="备注" name="remark" colProps={{ xs: 24 }} /> */}
        <Col span={24}>
          <CodeMirror
            height="800px"
            width="100"
            theme={'dark'}
            placeholder={'请输入您的代码模板...'}
            extensions={[loadLanguage('java')]}
          />
        </Col>
      </EditForm>
    </>
  );
};

export default Template;
