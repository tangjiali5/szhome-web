import type { ProColumns } from '@ant-design/pro-table';
import type { PropertyItem } from '@/services/develop/types';
import { Segmented, Tag } from 'antd';
import { getDictValues } from '@/services/system/dictValue';
import { AlignLeftOutlined, AlignCenterOutlined, AlignRightOutlined } from '@ant-design/icons';
import { ProFormDigitRange } from '@ant-design/pro-form';
/**
 * 步骤一
 */
const columns_1: ProColumns<PropertyItem>[] = [
  {
    title: '列名',
    dataIndex: 'columnName',
    width: 180,
    editable: false,
    ellipsis: true,
    copyable: true,
  },
  {
    title: '数据类型',
    dataIndex: 'columnType',
    width: 160,
    editable: false,
    ellipsis: true,
    render(_, entity) {
      // ZNBase驱动有问题，VARCHAR类型长度都是2147483647
      if (entity.columnLength > 2000000000) {
        return 'VARCHAR';
      }

      if (
        entity.columnType == 'TIMESTAMP' ||
        entity.columnType == 'DATETIME' ||
        entity.columnType == 'DATE' ||
        entity.columnType == 'TIME'
      ) {
        return entity.columnType;
      }

      let type = entity.columnType + '(' + entity.columnLength;
      if (!!entity.columnScale) {
        type += ', ' + entity.columnScale;
      }
      type += ')';

      return type;
    },
  },
  {
    title: '可空',
    width: 60,
    align: 'center',
    dataIndex: 'columnNullable',
    editable: false,
    render(dom, entity) {
      return (
        <span
          style={{
            color: entity.columnNullable ? 'var(--ant-primary-color)' : 'var(--ant-default-color)',
          }}
        >
          {entity.columnNullable ? '是' : '否'}
        </span>
      );
    },
  },
  {
    title: '默认值',
    dataIndex: 'columnDefaultValue',
    width: 160,
    ellipsis: true,
    editable: false,
  },
  {
    title: '列注释',
    dataIndex: 'columnComment',
    ellipsis: true,
    editable: false,
  },
  {
    title: '字段标签',
    dataIndex: 'fieldLabel',
    width: 160,
  },
  {
    title: '字段名称',
    dataIndex: 'fieldName',
    width: 160,
  },
  {
    title: '字段备注',
    dataIndex: 'fieldRemark',
    tooltip: '备注信息可用于描述字段含义',
    width: 260,
  },
];

const columns_2: ProColumns<PropertyItem>[] = [
  {
    title: '字段标签',
    dataIndex: 'fieldLabel',
    ellipsis: true,
    editable: false,
    width: 100,
  },
  {
    title: '字段名称',
    dataIndex: 'fieldName',
    editable: false,
    ellipsis: true,
    width: 100,
  },
  {
    title: '数据类型',
    dataIndex: 'columnType',
    width: 160,
    editable: false,
    ellipsis: true,
    render(_, entity) {
      // ZNBase驱动有问题，VARCHAR类型长度都是2147483647
      if (entity.columnLength > 2000000000) {
        return 'VARCHAR';
      }

      if (
        entity.columnType == 'TIMESTAMP' ||
        entity.columnType == 'DATETIME' ||
        entity.columnType == 'DATE' ||
        entity.columnType == 'TIME'
      ) {
        return entity.columnType;
      }

      let type = entity.columnType + '(' + entity.columnLength;
      if (!!entity.columnScale) {
        type += ', ' + entity.columnScale;
      }
      type += ')';

      return type;
    },
  },
  {
    title: '字段类型',
    dataIndex: 'fieldType',
    valueType: 'select',
    valueEnum: await getDictValues('java.type'),
    width: 180,
  },
  {
    title: 'JavaScript类型',
    dataIndex: 'jsType',
    valueType: 'select',
    valueEnum: await getDictValues('js.type'),
    width: 180,
  },
  {
    title: '取值示例',
    dataIndex: 'fieldExample',
    width: 200,
  },
  {
    title: '数据字典',
    dataIndex: 'fieldDict',
    width: 200,
  },
];

const columns_3: ProColumns<PropertyItem>[] = [
  {
    title: '字段名称',
    dataIndex: 'fieldLabel',
    editable: false,
  },
  {
    title: '字段编码',
    dataIndex: 'fieldName',
    editable: false,
  },
  {
    title: '前端组件',
    dataIndex: 'jsComponent',
    valueType: 'select',
    valueEnum: await getDictValues('form.component'),
    width: 180,
  },
  {
    title: '表单布局',
    dataIndex: 'formSpan',
    valueType: 'select',
    valueEnum: await getDictValues('form.columns'),
    width: 120,
  },
  {
    title: '查询条件',
    dataIndex: 'formSearchShow',
    tooltip: '字段是否作为查询条件',
    valueType: 'switch',
    align: 'center',
    width: 100,
  },
  {
    title: '匹配模式',
    dataIndex: 'formSearchMatch',
    tooltip: '字段作为查询条件的匹配方式，如等值匹配、模糊匹配等',
    valueType: 'select',
    valueEnum: await getDictValues('form.search.type'),
    width: 180,
  },
  {
    title: '列表展示',
    dataIndex: 'tableShow',
    tooltip: '字段是否作为表格列展示',
    valueType: 'switch',
    align: 'center',
    width: 100,
  },
  {
    title: '新增表单',
    dataIndex: 'formAddShow',
    tooltip: '字段是否在新增表单中展示',
    valueType: 'switch',
    align: 'center',
    width: 100,
  },
  {
    title: '编辑表单',
    dataIndex: 'formEditShow',
    tooltip: '字段是否在编辑表单中展示',
    valueType: 'switch',
    align: 'center',
    width: 100,
  },
  {
    title: '不可编辑',
    dataIndex: 'formEditReadonly',
    tooltip: '编辑表单展示但不可编辑的字段，将以只读形式展示',
    valueType: 'switch',
    align: 'center',
    width: 100,
  },
];

const columns_4: ProColumns<PropertyItem>[] = [
  {
    title: '字段标签',
    dataIndex: 'fieldLabel',
    editable: false,
  },
  {
    title: '字段名称',
    dataIndex: 'fieldName',
    editable: false,
  },
  {
    title: '校验规则',
    dataIndex: 'validateType',
    valueType: 'select',
    fieldProps: {
      mode: 'multiple',
      maxTagCount: 2,
    },
    valueEnum: await getDictValues('form.validation'),
    width: 290,
  },
  {
    title: '取值范围',
    dataIndex: 'validateValue',
    valueType: 'digitRange',
    align: 'center',
    width: 200,
  },
  {
    title: '字符长度',
    dataIndex: 'validateLength',
    valueType: 'digitRange',
    align: 'center',
    width: 200,
  },
  {
    title: '正则模式',
    dataIndex: 'validateRegex',
    align: 'center',
    width: 200,
  },
];

const columns_5: ProColumns<PropertyItem>[] = [
  {
    title: '字段标签',
    dataIndex: 'fieldLabel',
    editable: false,
  },
  {
    title: '字段名称',
    dataIndex: 'fieldName',
    editable: false,
  },
  {
    title: '列宽度',
    dataIndex: 'tableWidth',
    width: 180,
  },
  {
    title: '对齐方式',
    dataIndex: 'tableAlign',
    align: 'center',
    width: 150,
    renderFormItem() {
      return (
        <Segmented
          options={[
            {
              label: <AlignLeftOutlined style={{ color: 'var(--ant-primary-color)' }} />,
              value: 'left',
            },
            {
              label: <AlignCenterOutlined style={{ color: 'var(--ant-primary-color)' }} />,
              value: 'center',
            },
            {
              label: <AlignRightOutlined style={{ color: 'var(--ant-primary-color)' }} />,
              value: 'right',
            },
          ]}
        />
      );
    },
  },
  {
    title: '冻结列',
    dataIndex: 'fixed',
    valueType: 'select',
    valueEnum: await getDictValues('table.fixed'),
    align: 'center',
    width: 120,
  },
  {
    title: '溢出省略',
    dataIndex: 'tableEllipsis',
    valueType: 'switch',
    tooltip: '文本过长时是否以省略号显示，此时可以通过鼠标悬停查看完整内容',
    align: 'center',
    width: 100,
  },
  {
    title: '支持复制',
    dataIndex: 'tableCopyable',
    valueType: 'switch',
    align: 'center',
    width: 100,
  },
  {
    title: '查询表单权重',
    dataIndex: 'formSearchSort',
    valueType: 'digit',
    tooltip: '控制字段在查询表单中的顺序，权重越大越靠前',
    width: 120,
  },
  {
    title: '编辑表单顺序',
    dataIndex: 'formEditSort',
    valueType: 'digit',
    tooltip: '控制字段在新增、编辑表单中的顺序，数值越小越靠前',
    width: 120,
  },
];

/**
 * 默认列
 */
const defaultColumns: ProColumns<PropertyItem>[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    hideInSearch: true,
    hideInTable: true,
  },
  {
    title: '#',
    hideInSearch: true,
    width: 60,
    align: 'center',
    editable: false,
    render(_, __, index, action) {
      const current = action?.pageInfo?.current || 1;
      const pageSize = action?.pageInfo?.pageSize || 10;
      return (current - 1) * pageSize + index + 1;
    },
  },
];

/**
 * 根据步骤设置列信息
 *
 * @param step 步骤
 * @returns
 */
export const getColumns = (step: number) => {
  const columns: ProColumns<PropertyItem>[] = [
    ...defaultColumns,
    ...[columns_1, columns_2, columns_3, columns_4, columns_5][step],
    {
      title: '操作',
      align: 'center',
      dataIndex: 'option',
      valueType: 'option',
      width: 60,
      render: () => null,
    },
  ];
  return columns;
};
