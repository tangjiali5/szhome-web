import type { DatasourceItem } from '@/services/develop/types';
import {
  addDatasource,
  updateDatasource,
  removeDatasource,
  syncDatasource,
} from '@/services/develop/datasource';
import { message } from 'antd';

/**
 * 添加数据源
 *
 * @param fields
 */
export const handleAdd = async (fields: DatasourceItem) => {
  const hide = message.loading('正在新增数据源');

  try {
    await addDatasource({ ...fields });
    hide();
    message.success('数据源添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('数据源添加失败，请重试！');
    return false;
  }
};

/**
 * 更新数据源
 *
 * @param fields
 */
export const handleUpdate = async (fields: DatasourceItem, currentRow?: DatasourceItem) => {
  const hide = message.loading('正在更新数据源');

  try {
    await updateDatasource({
      ...currentRow,
      ...fields,
    });
    hide();
    message.success('数据源更新成功');
    return true;
  } catch (error) {
    hide();
    message.error('数据源更新失败，请重试');
    return false;
  }
};

/**
 * 删除数据源
 *
 * @param selectedRows
 */
export const handleRemove = async (selectedRows: DatasourceItem[]) => {
  const hide = message.loading('正在删除数据源');
  if (!selectedRows) return true;

  try {
    await removeDatasource(selectedRows.map((row) => row.id));
    hide();
    message.success('数据源删除成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('数据源删除失败，请重试');
    return false;
  }
};

/**
 * 同步数据源
 *
 * @param selectedRows
 */
export const handleSync = async (selectedRows: DatasourceItem[]) => {
  const hide = message.loading('正在同步数据源');
  if (!selectedRows) return true;

  try {
    await syncDatasource(selectedRows.map((row) => row.id));
    hide();
    message.success('数据源同步成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('数据源同步失败，请重试');
    return false;
  }
};
