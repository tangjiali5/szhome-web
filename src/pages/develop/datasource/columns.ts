import type { ProColumns } from '@ant-design/pro-table';
import type { DatasourceItem } from '@/services/develop/types';
import { getDictValues } from '@/services/system/dictValue';

export const DatasourceColumns: ProColumns<DatasourceItem>[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    hideInSearch: true,
    hideInTable: true,
  },
  {
    title: '#',
    hideInSearch: true,
    width: 60,
    align: 'center',
    render(_, __, index, action) {
      const current = action?.pageInfo?.current || 1;
      const pageSize = action?.pageInfo?.pageSize || 10;
      return (current - 1) * pageSize + index + 1;
    },
  },
  {
    title: '名称',
    dataIndex: 'name',
  },
  {
    title: '类型',
    dataIndex: 'type',
    valueType: 'select',
    valueEnum: await getDictValues('database.type'),
  },
  {
    title: '连接地址',
    dataIndex: 'url',
    ellipsis: true,
    copyable: true,
  },
  {
    title: '用户名',
    dataIndex: 'username',
  },
  {
    title: '密码',
    dataIndex: 'password',
    hideInSearch: true,
    hideInTable: true,
  },
  {
    title: '表前缀',
    dataIndex: 'tablePrefix',
    hideInSearch: true,
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
];
