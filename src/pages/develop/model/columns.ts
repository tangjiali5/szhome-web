import type { ProColumns } from '@ant-design/pro-table';
import type { ModelItem } from '@/services/develop/types';

export const ModelColumns: ProColumns<ModelItem>[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    hideInSearch: true,
    hideInTable: true,
  },
  {
    title: '#',
    hideInSearch: true,
    width: 60,
    align: 'center',
    render(_, __, index, action) {
      const current = action?.pageInfo?.current || 1;
      const pageSize = action?.pageInfo?.pageSize || 10;
      return (current - 1) * pageSize + index + 1;
    },
  },
  {
    title: '表名',
    dataIndex: 'tableName',
    width: 220,
    ellipsis: true,
    copyable: true,
  },
  {
    title: '数据源',
    dataIndex: 'datasourceId',
    width: 200,
    ellipsis: true,
  },
  {
    title: '表前缀',
    width: 120,
    dataIndex: 'tablePrefix',
  },
  {
    title: '表注释',
    dataIndex: 'comments',
    ellipsis: true,
  },
  {
    title: '模型编码',
    dataIndex: 'code',
    hideInTable: true,
  },
  {
    title: '模型名称',
    dataIndex: 'name',
    width: 120,
    ellipsis: true,
  },
  {
    title: '模块编码',
    dataIndex: 'module',
    hideInTable: true,
  },
  {
    title: '作者',
    dataIndex: 'author',
    hideInTable: true,
  },
  {
    title: '树形模型',
    dataIndex: 'tree',
    hideInTable: true,
  },
  {
    title: '备注',
    dataIndex: 'remark',
    hideInTable: true,
  },
];
