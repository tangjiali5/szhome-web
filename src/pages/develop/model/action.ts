import type { ModelItem } from '@/services/develop/types';
import { addModel, updateModel, removeModel } from '@/services/develop/model';
import { message } from 'antd';

/**
 * 添加模型
 *
 * @param fields
 */
export const handleAdd = async (fields: ModelItem) => {
  const hide = message.loading('正在新增模型');

  try {
    await addModel({ ...fields });
    hide();
    message.success('模型添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('模型添加失败，请重试！');
    return false;
  }
};

/**
 * 更新模型
 *
 * @param fields
 */
export const handleUpdate = async (fields: ModelItem, currentRow?: ModelItem) => {
  const hide = message.loading('正在更新模型');

  try {
    await updateModel({
      ...currentRow,
      ...fields,
    });
    hide();
    message.success('模型更新成功');
    return true;
  } catch (error) {
    hide();
    message.error('模型更新失败，请重试');
    return false;
  }
};

/**
 * 删除模型
 *
 * @param selectedRows
 */
export const handleRemove = async (selectedRows: ModelItem[]) => {
  const hide = message.loading('正在删除模型');
  if (!selectedRows) return true;

  try {
    await removeModel(selectedRows.map((row) => row.id));
    hide();
    message.success('模型删除成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('模型删除失败，请重试');
    return false;
  }
};
