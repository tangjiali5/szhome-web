import { FileExclamationOutlined } from '@ant-design/icons';
import { Spin, Tabs, message } from 'antd';
import React, { useEffect, useState } from 'react';
import Script from '../script/Script';
import { generateScript } from '@/services/develop/generator';

// 标签列表
const items = [
  {
    label: 'SQL脚本',
    icon: <FileExclamationOutlined />,
    type: 'sql',
    children: [
      {
        label: 'Mysql',
        template: 'mysql',
      },
      {
        label: 'PgSQL',
        template: 'pgsql',
      },
      {
        label: 'Oracle',
        template: 'oracle',
      },
      {
        label: 'MSServer',
        template: 'msserver',
      },
      {
        label: 'DB2',
        template: 'db2',
      },
    ],
  },
  {
    label: 'Java代码',
    icon: <FileExclamationOutlined />,
    type: 'java',
    children: [
      {
        label: 'Lombok版',
        template: 'lombok',
      },
      {
        label: 'Java版',
        template: 'java',
      },
      {
        label: 'Kotlin版',
        template: 'kotlin',
      },
    ],
  },
  {
    label: '前端类型',
    icon: <FileExclamationOutlined />,
    type: 'jsx',
    children: [
      {
        label: '模型Type',
        template: 'data',
      },
      {
        label: '查询参数',
        template: 'param',
      },
    ],
  },
];

type ModelScriptProps = {
  modelId: string;
};

const ModelScript: React.FC<ModelScriptProps> = (props) => {
  const [code, setCode] = useState<string>();
  const [loading, setLoading] = useState<boolean>();

  const getScript = (modelId: string, key: string) => {
    setLoading(true);
    generateScript({
      modelId: modelId,
      templateCode: key,
    })
      .then((res) => {
        setCode(res.data);
      })
      .catch(() => {
        // message.error(err.message);
        setCode('');
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    getScript(props.modelId, 'mysql');
  }, [props.modelId]);

  // 生成多个标签页
  const tabs = items.map((item: any, index: number) => {
    return {
      key: 'tab-' + index,
      label: (
        <>
          {item.icon ? item.icon : <FileExclamationOutlined />}
          &nbsp;&nbsp;{item.label}
        </>
      ),
      children:
        item.children != null && item.children.length > 0 ? (
          <Tabs
            onChange={(key) => getScript(props.modelId, key)}
            tabPosition="left"
            items={item.children.map((child: any) => {
              return {
                key: child.template,
                label: child.label,
                children: (
                  <Spin spinning={loading}>
                    <Script code={code} type={child.type} />
                  </Spin>
                ),
              };
            })}
          />
        ) : (
          <Spin spinning={loading}>
            <Script code={code} type={item.type} />
          </Spin>
        ),
    };
  });

  return <Tabs type="card" items={tabs} />;
};

export default ModelScript;
