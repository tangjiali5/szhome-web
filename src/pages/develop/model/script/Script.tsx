import React from 'react';
import { loadLanguage } from '@uiw/codemirror-extensions-langs';
import type { ReactCodeMirrorProps } from '@uiw/react-codemirror';
import CodeMirror from '@uiw/react-codemirror';

type ScriptProps = {
  type: string;
  code: string | undefined;
} & ReactCodeMirrorProps;

/**
 * 脚本显示器
 * @param props
 * @returns
 */
const Script: React.FC<ScriptProps> = (props) => {
  return (
    <CodeMirror
      value={props.code}
      height="100%"
      minHeight="100px"
      maxHeight="800px"
      width="100"
      theme={'light'}
      readOnly={true}
      // onChange={setTemplateContent}
      placeholder={'请输入您的模板代码...'}
      extensions={[loadLanguage('java')]}
      {...props}
    />
  );
};

export default Script;
