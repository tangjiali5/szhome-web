import {
  PlusOutlined,
  DeleteOutlined,
  CloudDownloadOutlined,
  CloudUploadOutlined,
} from '@ant-design/icons';
import { Button, Popconfirm } from 'antd';
import React, { useState, useRef } from 'react';
import { PageContainer } from '@ant-design/pro-layout';
import type { ProColumns, ActionType } from '@ant-design/pro-table';
import type { ProjectItem, ProjectParams } from '@/services/develop/types';
import { ProFormText, ProFormTextArea } from '@ant-design/pro-form';
import EditForm from '@/components/EditForm';
import { ProjectColumns } from './columns';
import { handleAdd, handleUpdate, handleRemove } from './action';
import { pageProject } from '@/services/develop/project';
import SzhomeTable from '@/components/Custom/SzhomeTable';
import SzhomeSelect from '@/components/Custom/SzhomeSelect';
import { listTemplateSchema } from '@/services/develop/templateSchema';

const Project: React.FC = () => {
  const [createModalVisible, handleModalVisible] = useState<boolean>(false);
  const actionRef = useRef<ActionType>();
  const [currentRow, setCurrentRow] = useState<ProjectItem>();
  const [selectedRowsState, setSelectedRows] = useState<ProjectItem[]>([]);

  const columns: ProColumns<ProjectItem>[] = [
    ...ProjectColumns,
    {
      title: '操作',
      align: 'center',
      dataIndex: 'option',
      valueType: 'option',
      width: 220,
      render: (_, record) => [
        <a
          key={'edit-' + record.id}
          onClick={() => {
            setCurrentRow(record);
            handleModalVisible(true);
          }}
        >
          编辑
        </a>,
        <Popconfirm
          key={'delete-' + record.id}
          title="您确定要删除此工程吗？"
          onConfirm={async () => {
            await handleRemove([record]);
            setSelectedRows([]);
            actionRef.current?.reloadAndRest?.();
          }}
          okText="Yes"
          cancelText="No"
        >
          <a key="remove">删除</a>
        </Popconfirm>,
      ],
    },
  ];

  return (
    <PageContainer>
      <SzhomeTable<ProjectItem, ProjectParams>
        actionRef={actionRef}
        request={pageProject}
        columns={columns}
        toolBarRender={() => [
          <Button
            type="primary"
            key="primary"
            onClick={() => {
              setCurrentRow(undefined);
              handleModalVisible(true);
            }}
          >
            <PlusOutlined /> 新增
          </Button>,
          <Popconfirm
            key={'batch_remove'}
            disabled={selectedRowsState?.length == 0}
            title={'您确定要删除已选' + selectedRowsState?.length + '条工程数据吗？'}
            onConfirm={async () => {
              await handleRemove(selectedRowsState);
              setSelectedRows([]);
              actionRef.current?.reloadAndRest?.();
            }}
            okText="Yes"
            cancelText="No"
          >
            <Button key="batch_remove" danger disabled={selectedRowsState?.length == 0}>
              <DeleteOutlined />
              删除
            </Button>
          </Popconfirm>,

          <Button
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <CloudDownloadOutlined /> 导出
          </Button>,

          <Button
            key="primary"
            onClick={() => {
              handleModalVisible(true);
            }}
          >
            <CloudUploadOutlined /> 导入
          </Button>,
        ]}
        rowSelection={{
          onChange: (_, selectedRows) => {
            setSelectedRows(selectedRows);
          },
        }}
      />

      <EditForm
        visible={createModalVisible}
        name="工程"
        model={currentRow || { id: undefined }}
        onAdd={handleAdd}
        onUpdate={handleUpdate}
        afterEdit={() => {
          setCurrentRow(undefined);

          if (actionRef.current) {
            actionRef.current.reload();
          }
        }}
        onCancel={handleModalVisible}
      >
        <ProFormText name="id" hidden={true} />
        <ProFormText required label="工程名称" name="name" colProps={{ xs: 12 }} />
        <ProFormText required label="工程编码" name="code" colProps={{ xs: 12 }} />
        <ProFormText required label="基础包路径" name="basePkg" colProps={{ xs: 12 }} />
        <SzhomeSelect
          required
          label="模板方案"
          name="templateSchemaId"
          fn={listTemplateSchema}
          colProps={{ xs: 12 }}
        />
        <ProFormTextArea label="备注" name="remark" />
      </EditForm>
    </PageContainer>
  );
};

export default Project;
