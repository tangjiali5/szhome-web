import type { ProColumns } from '@ant-design/pro-table';
import type { ProjectItem } from '@/services/develop/types';

export const ProjectColumns: ProColumns<ProjectItem>[] = [
  {
    title: 'ID',
    dataIndex: 'id',
    hideInSearch: true,
    hideInTable: true,
  },
  {
    title: '#',
    hideInSearch: true,
    width: 60,
    align: 'center',
    render(_, __, index, action) {
      const current = action?.pageInfo?.current || 1;
      const pageSize = action?.pageInfo?.pageSize || 10;
      return (current - 1) * pageSize + index + 1;
    },
  },
  {
    title: '工程名称',
    dataIndex: 'name',
    valueType: 'select',
    request: () => {
      return Promise.resolve();
    },
    // params: {},
    fieldProps: {
      fieldNames: {
        label: 'name',
        value: 'id',
      },
    },
  },
  {
    title: '工程编码',
    dataIndex: 'code',
  },
  {
    title: '基础包路径',
    dataIndex: 'basePkg',
  },
  {
    title: '备注',
    dataIndex: 'remark',
  },
];
