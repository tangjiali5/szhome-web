import type { ProjectItem } from '@/services/develop/types';
import { addProject, updateProject, removeProject } from '@/services/develop/project';
import { message } from 'antd';

/**
 * 添加工程
 *
 * @param fields
 */
export const handleAdd = async (fields: ProjectItem) => {
  const hide = message.loading('正在新增工程');

  try {
    await addProject({ ...fields });
    hide();
    message.success('工程添加成功');
    return true;
  } catch (error) {
    hide();
    message.error('工程添加失败，请重试！');
    return false;
  }
};

/**
 * 更新工程
 *
 * @param fields
 */
export const handleUpdate = async (fields: ProjectItem, currentRow?: ProjectItem) => {
  const hide = message.loading('正在更新工程');

  try {
    await updateProject({
      ...currentRow,
      ...fields,
    });
    hide();
    message.success('工程更新成功');
    return true;
  } catch (error) {
    hide();
    message.error('工程更新失败，请重试');
    return false;
  }
};

/**
 * 删除工程
 *
 * @param selectedRows
 */
export const handleRemove = async (selectedRows: ProjectItem[]) => {
  const hide = message.loading('正在删除工程');
  if (!selectedRows) return true;

  try {
    await removeProject(selectedRows.map((row) => row.id));
    hide();
    message.success('工程删除成功，即将刷新');
    return true;
  } catch (error) {
    hide();
    message.error('工程删除失败，请重试');
    return false;
  }
};
