/**
 * 分页参数
 */
export type Pagination = {
  total?: number;
  pageSize: number;
  current: number;
};

/**
 * 表格数据结构
 */
export type Data<T> = {
  data: T[];
  total?: number;
  pageSize?: number;
  current?: number;
};
