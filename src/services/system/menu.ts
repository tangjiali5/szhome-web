// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { MenuItem, MenuParams } from '@/services/system/types';
import { Data } from '@/services/types';

/**
 * API根路径
 */
const CONTEXT_PATH = '/api/szhome-system/system/menu/';

/**
 * 分页查询菜单列表
 *
 * @param params 菜单分页查询条件
 * @returns 返回菜单列表
 */
export async function pageMenu(params: MenuParams) {
  return request<Data<MenuItem>>(CONTEXT_PATH + 'page', {
    method: 'GET',
    params,
  });
}

/** 新建菜单 */
export async function addMenu(data: MenuItem) {
  return request<MenuItem>(CONTEXT_PATH + 'add', {
    method: 'POST',
    data,
  });
}

/**
 * 更新菜单信息
 *
 * @param data 菜单信息
 * @returns
 */
export async function updateMenu(data: MenuItem) {
  return request<MenuItem>(CONTEXT_PATH + 'update', {
    method: 'POST',
    data,
  });
}

/**
 * 删除菜单
 *
 * @param ids 菜单ID
 * @returns
 */
export async function removeMenu(ids: Array<number | string>) {
  return request<Record<string, any>>(CONTEXT_PATH + 'delete', {
    method: 'POST',
    data: ids,
  });
}

/**
 * 获取组织架构树
 *
 * @param params
 * @param options
 * @returns
 */
export async function treeMenu(params: {}) {
  return await request<Data<MenuItem>>(CONTEXT_PATH + 'tree', {
    method: 'GET',
    params,
  });
}

/**
 * 获取角色分配的菜单
 *
 * @param roleId
 * @returns
 */
export async function getMenusOfRole(roleId: string | number) {
  return await request<Data<MenuItem>>(CONTEXT_PATH + 'role-menu', {
    method: 'GET',
    params: {
      roleId,
    },
  });
}
