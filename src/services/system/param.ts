// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { ParamItem, ParamParams } from '@/services/system/types';
import { Data } from '@/services/types';

/**
 * API根路径
 */
const CONTEXT_PATH = '/api/szhome-system/system/param/';

/**
 * 分页查询系统参数列表
 *
 * @param params 系统参数分页查询条件
 * @returns 返回系统参数列表
 */
export async function pageParam(params: ParamParams) {
  return request<Data<ParamItem>>(CONTEXT_PATH + 'page', {
    method: 'GET',
    params,
  });
}

/** 新建系统参数 */
export async function addParam(data: ParamItem) {
  return request<ParamItem>(CONTEXT_PATH + 'add', {
    method: 'POST',
    data,
  });
}

/**
 * 更新系统参数信息
 *
 * @param data 系统参数信息
 * @returns
 */
export async function updateParam(data: ParamItem) {
  return request<ParamItem>(CONTEXT_PATH + 'update', {
    method: 'POST',
    data,
  });
}

/**
 * 删除系统参数
 *
 * @param ids 系统参数ID
 * @returns
 */
export async function removeParam(ids: Array<number | string>) {
  return request<Record<string, any>>(CONTEXT_PATH + 'delete', {
    method: 'POST',
    data: ids,
  });
}

/**
 * 获取组织架构树
 *
 * @param params
 * @param options
 * @returns
 */
export async function treeParam(params: {}) {
  return await request<Data<ParamItem>>(CONTEXT_PATH + 'tree', {
    method: 'GET',
    params,
  });
}
