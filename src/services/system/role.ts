// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { RoleItem, RoleParams } from '@/services/system/types';
import { Data } from '@/services/types';
import { Key } from 'react';

/**
 * API根路径
 */
const CONTEXT_PATH = '/api/szhome-system/system/role/';

/**
 * 分页查询角色列表
 *
 * @param params 角色分页查询条件
 * @returns 返回角色列表
 */
export async function pageRole(params: RoleParams) {
  return request<Data<RoleItem>>(CONTEXT_PATH + 'page', {
    method: 'GET',
    params,
  });
}

/** 新建角色 */
export async function addRole(data: RoleItem) {
  return request<RoleItem>(CONTEXT_PATH + 'add', {
    method: 'POST',
    data,
  });
}

/**
 * 更新角色信息
 *
 * @param data 角色信息
 * @returns
 */
export async function updateRole(data: RoleItem) {
  return request<RoleItem>(CONTEXT_PATH + 'update', {
    method: 'POST',
    data,
  });
}

/**
 * 删除角色
 *
 * @param ids 角色ID
 * @returns
 */
export async function removeRole(ids: Array<number | string>) {
  return request<Record<string, any>>(CONTEXT_PATH + 'delete', {
    method: 'POST',
    data: ids,
  });
}

/**
 * 分页查询角色列表
 *
 * @param params 角色分页查询条件
 * @returns 返回角色列表
 */
export async function listRole(params: RoleParams) {
  return request<Data<RoleItem>>(CONTEXT_PATH + 'list', {
    method: 'GET',
    params,
  });
}

/**
 * 获取组织架构树
 *
 * @param params
 * @param options
 * @returns
 */
export async function treeRole(params: {}) {
  return await request<Data<RoleItem>>(CONTEXT_PATH + 'tree', {
    method: 'GET',
    params,
  });
}

/**
 * 为角色分配菜单
 *
 * @param roleIds   角色列表
 * @param menuIds   菜单列表
 * @returns
 */
export async function grantMenus(
  roleIds: number[] | string[],
  menuIds: Key[] | { checked: Key[]; halfChecked: Key[] },
) {
  return request(CONTEXT_PATH + 'grant-menus', {
    method: 'POST',
    params: {
      roleIds,
      menuIds,
    },
  });
}
