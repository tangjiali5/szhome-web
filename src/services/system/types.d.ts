/***** 账户 *****/
export type UserItem = {
  id: string;
  username: string;
  password: string;
  realName: string;
  avatar: string;
  email: string;
  phone: string;
  birthday: LocalDate;
  gender: string;
  status: string;
  deptId: number;
  tenantId: string;
  createUser: string;
  createTime: date;
  updateUser: string;
  updateTime: date;
};

export type UserParams = {
  id?: string;
  username?: string;
  password?: string;
  realName?: string;
  avatar?: string;
  email?: string;
  phone?: string;
  birthday?: LocalDate;
  gender?: string;
  status?: string;
  deptId?: number;
  tenantId?: string;
  createUser?: string;
  createTime?: date;
  updateUser?: string;
  updateTime?: date;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};

/***** 角色 *****/
export type RoleItem = {
  id: string;
  name: string;
  remark: string;
  sort: number;
  tenantId: string;
  createUser: string;
  createTime: date;
  updateUser: string;
  updateTime: date;
};

export type RoleParams = {
  id?: string;
  name?: string;
  remark?: string;
  sort?: number;
  tenantId?: string;
  createUser?: string;
  createTime?: date;
  updateUser?: string;
  updateTime?: date;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};

/***** 菜单 *****/
export type MenuItem = {
  id: string;
  parentId: number | string;
  code: string;
  name: string;
  path: string;
  perms: string;
  icon: string;
  type: string;
  visible: string;
  remark: string;
  status: string;
  sort: number;
  createUser: string;
  createTime: date;
  updateUser: string;
  updateTime: date;
};

export type MenuParams = {
  id?: string;
  parentId?: number;
  code?: string;
  name?: string;
  path?: string;
  perms?: string;
  icon?: string;
  type?: string;
  visible?: string;
  remark?: string;
  status?: string;
  sort?: number;
  createUser?: string;
  createTime?: date;
  updateUser?: string;
  updateTime?: date;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};

/***** 部门 *****/
export type DeptItem = {
  id: string;
  parentId?: string;
  ancestors?: string;
  name?: string;
  type?: string;
  ownerId?: number;
  remark?: string;
  status?: string;
  sort?: number;
  children?: DeptItem[];
  createUser?: string;
  createTime?: Date;
  updateUser?: string;
  updateTime?: Date;
};

export type DeptParams = {
  id?: number;
  parentId?: number;
  ancestors?: string;
  name?: string;
  type?: string;
  ownerId?: number;
  remark?: string;
  status?: string;
  sort?: number;
  createUser?: string;
  createTime?: Date;
  updateUser?: string;
  updateTime?: Date;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};

/***** 岗位 *****/
export type PostItem = {
  id: string;
  code: string;
  name: string;
  remark: string;
  status: string;
  sort: number;
  createUser: string;
  createTime: date;
  updateUser: string;
  updateTime: date;
};

export type PostParams = {
  id?: string;
  code?: string;
  name?: string;
  remark?: string;
  status?: string;
  sort?: number;
  createUser?: string;
  createTime?: date;
  updateUser?: string;
  updateTime?: date;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};

/***** 数据字典 *****/
export type DictItem = {
  id: string;
  name?: string;
  code?: string;
  remark?: string;
  createUser?: string;
  createTime?: date;
  updateUser?: string;
  updateTime?: date;
};

export type DictParams = {
  id?: string;
  name?: string;
  code?: string;
  remark?: string;
  createUser?: string;
  createTime?: date;
  updateUser?: string;
  updateTime?: date;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};

/***** 数据字典项 *****/
export type DictValueItem = {
  id?: string;
  dictCode?: string;
  name?: string;
  value?: string;
  color?: string;
  defaultValue?: boolean;
  status?: string;
  sort?: number;
  remark?: string;
  createUser?: string;
  createTime?: date;
  updateUser?: string;
  updateTime?: date;
};

export type DictValueParams = {
  id?: string;
  dictCode?: string;
  name?: string;
  value?: string;
  color?: string;
  defaultValue?: boolean;
  status?: string;
  sort?: number;
  remark?: string;
  createUser?: string;
  createTime?: date;
  updateUser?: string;
  updateTime?: date;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};

/***** 系统参数 *****/
export type ParamItem = {
  id: string;
  name: string;
  code: string;
  value: string;
  remark: string;
  createUser: string;
  createTime: date;
  updateUser: string;
  updateTime: date;
  deleted: Long;
};

export type ParamParams = {
  id?: string;
  name?: string;
  code?: string;
  value?: string;
  remark?: string;
  createUser?: string;
  createTime?: date;
  updateUser?: string;
  updateTime?: date;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};
