// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { DictValueItem, DictValueParams } from '@/services/system/types';
import { Data } from '@/services/types';

/**
 * API根路径
 */
const CONTEXT_PATH = '/api/szhome-system/system/dictValue/';

/**
 * 分页查询数据字典项列表
 *
 * @param params 数据字典项分页查询条件
 * @returns 返回数据字典项列表
 */
export async function pageDictValue(params: DictValueParams) {
  return request<Data<DictValueItem>>(CONTEXT_PATH + 'page', {
    method: 'GET',
    params,
  });
}

/** 新建数据字典项 */
export async function addDictValue(data: DictValueItem) {
  return request<DictValueItem>(CONTEXT_PATH + 'add', {
    method: 'POST',
    data,
  });
}

/**
 * 更新数据字典项信息
 *
 * @param data 数据字典项信息
 * @returns
 */
export async function updateDictValue(data: DictValueItem) {
  return request<DictValueItem>(CONTEXT_PATH + 'update', {
    method: 'POST',
    data,
  });
}

/**
 * 删除数据字典项
 *
 * @param ids 数据字典项ID
 * @returns
 */
export async function removeDictValue(ids: Array<number | string | undefined>) {
  return request<Record<string, any>>(CONTEXT_PATH + 'delete', {
    method: 'POST',
    data: ids,
  });
}

/**
 * 分页查询数据字典项列表
 *
 * @param params 数据字典项分页查询条件
 * @returns 返回数据字典项列表
 */
export async function listDictValue(params: DictValueParams) {
  return request<Data<DictValueItem>>(CONTEXT_PATH + 'list', {
    method: 'GET',
    params,
  });
}

/**
 *  获取数据字典项
 *
 * @param dictCode
 * @param options
 * @returns
 */
export async function getDictValues(dictCode: string, colorful: boolean = true) {
  const res = await request<Data<DictValueItem>>(CONTEXT_PATH + 'list', {
    method: 'GET',
    params: {
      dictCode,
    },
  });

  if (colorful) {
    let result = {};
    res.data.map((value) => {
      if (value.value) {
        result[value.value] = {
          text: value.name,
          color: value.color,
        };
      }
    });
    return result;
  }

  return res.data.map((value) => {
    return {
      label: value.name,
      value: value.value,
      status: 'Success',
      color: value.color,
    };
  });
}
