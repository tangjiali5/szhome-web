// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { DictItem, DictParams } from '@/services/system/types';
import { Data } from '@/services/types';

/**
 * API根路径
 */
const CONTEXT_PATH = '/api/szhome-system/system/dict/';

/**
 * 分页查询字典列表
 *
 * @param params 字典分页查询条件
 * @returns 返回字典列表
 */
export async function pageDict(params: DictParams) {
  return request<Data<DictItem>>(CONTEXT_PATH + 'page', {
    method: 'GET',
    params,
  });
}

/**
 * 新增字典
 *
 * @param data 字典信息
 * @returns
 */
export async function addDict(data: DictItem) {
  return request<DictItem>(CONTEXT_PATH + 'add', {
    method: 'POST',
    data,
  });
}

/**
 * 更新字典信息
 *
 * @param data 字典信息
 * @returns
 */
export async function updateDict(data: DictItem) {
  return request<DictItem>(CONTEXT_PATH + 'update', {
    method: 'POST',
    data,
  });
}

/**
 * 删除字典
 *
 * @param ids 字典ID
 * @returns
 */
export async function removeDict(ids: Array<number | string>) {
  return request<Record<string, any>>(CONTEXT_PATH + 'delete', {
    method: 'POST',
    data: ids,
  });
}
