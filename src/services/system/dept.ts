// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { DeptItem, DeptParams } from '@/services/system/types';
import { Data } from '@/services/types';

/**
 * API根路径
 */
const CONTEXT_PATH = '/api/szhome-system/system/dept/';

/**
 * 分页查询部门列表
 *
 * @param params 部门分页查询条件
 * @returns 返回部门列表
 */
export async function pageDept(params: DeptParams) {
  return request<Data<DeptItem>>(CONTEXT_PATH + 'page', {
    method: 'GET',
    params,
  });
}

/** 新建部门 */
export async function addDept(data: DeptItem) {
  return request<DeptItem>(CONTEXT_PATH + 'add', {
    method: 'POST',
    data,
  });
}

/**
 * 更新部门信息
 *
 * @param data 部门信息
 * @returns
 */
export async function updateDept(data: DeptItem) {
  return request<DeptItem>(CONTEXT_PATH + 'update', {
    method: 'POST',
    data,
  });
}

/**
 * 删除部门
 *
 * @param ids 部门ID
 * @returns
 */
export async function removeDept(ids: Array<number | string>) {
  return request<Record<string, any>>(CONTEXT_PATH + 'delete', {
    method: 'POST',
    data: ids,
  });
}

/**
 * 获取组织架构树
 *
 * @param params
 * @param options
 * @returns
 */
export async function treeDept(params: {}) {
  return await request<Data<DeptItem>>(CONTEXT_PATH + 'tree', {
    method: 'GET',
    params,
  });
}
