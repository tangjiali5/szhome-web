// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { PostItem, PostParams } from '@/services/system/types';
import { Data } from '@/services/types';

/**
 * API根路径
 */
const CONTEXT_PATH = '/api/szhome-system/system/post/';

/**
 * 分页查询岗位列表
 *
 * @param params 岗位分页查询条件
 * @returns 返回岗位列表
 */
export async function pagePost(params: PostParams) {
  return request<Data<PostItem>>(CONTEXT_PATH + 'page', {
    method: 'GET',
    params,
  });
}

/** 新建岗位 */
export async function addPost(data: PostItem) {
  return request<PostItem>(CONTEXT_PATH + 'add', {
    method: 'POST',
    data,
  });
}

/**
 * 更新岗位信息
 *
 * @param data 岗位信息
 * @returns
 */
export async function updatePost(data: PostItem) {
  return request<PostItem>(CONTEXT_PATH + 'update', {
    method: 'POST',
    data,
  });
}

/**
 * 删除岗位
 *
 * @param ids 岗位ID
 * @returns
 */
export async function removePost(ids: Array<number | string>) {
  return request<Record<string, any>>(CONTEXT_PATH + 'delete', {
    method: 'POST',
    data: ids,
  });
}

/**
 * 获取组织架构树
 *
 * @param params
 * @param options
 * @returns
 */
export async function treePost(params: {}) {
  return await request<Data<PostItem>>(CONTEXT_PATH + 'tree', {
    method: 'GET',
    params,
  });
}
