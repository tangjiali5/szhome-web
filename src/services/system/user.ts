// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { UserItem, UserParams } from '@/services/system/types';
import { Data } from '@/services/types';

/**
 * API根路径
 */
const CONTEXT_PATH = '/api/szhome-system/system/user/';

/**
 * 分页查询用户列表
 *
 * @param params 用户分页查询条件
 * @returns 返回用户列表
 */
export async function pageUser(params: UserParams) {
  return request<Data<UserItem>>(CONTEXT_PATH + 'page', {
    method: 'GET',
    params,
  });
}

/** 新建用户 */
export async function addUser(data: UserItem) {
  return request<UserItem>(CONTEXT_PATH + 'add', {
    method: 'POST',
    data,
  });
}

/**
 * 更新用户信息
 *
 * @param data 用户信息
 * @returns
 */
export async function updateUser(data: UserItem) {
  return request<UserItem>(CONTEXT_PATH + 'update', {
    method: 'POST',
    data,
  });
}

/**
 * 删除用户
 *
 * @param ids 用户ID
 * @returns
 */
export async function removeUser(ids: Array<number | string>) {
  return request<Record<string, any>>(CONTEXT_PATH + 'delete', {
    method: 'POST',
    data: ids,
  });
}

/**
 * 获取组织架构树
 *
 * @param params
 * @param options
 * @returns
 */
export async function treeUser(params: {}) {
  return await request<Data<UserItem>>(CONTEXT_PATH + 'tree', {
    method: 'GET',
    params,
  });
}

/**
 * 用户列表
 *
 * @param params
 * @returns
 */
export async function listUser(params: {}) {
  const res = await request<Data<UserItem>>(CONTEXT_PATH + 'list', {
    method: 'GET',
    params,
  });

  return res.data.map((item: UserItem) => {
    return {
      label: item.realName,
      value: item.id,
    };
  });
}

/**
 * 重置用户密码
 * @param ids 用户ID
 * @returns
 */
export async function resetPwd(ids: Array<number | string>) {
  return request<Record<string, any>>(CONTEXT_PATH + 'reset-password', {
    method: 'POST',
    data: ids,
  });
}

/**
 * 获取用户已分配的角色列表
 *
 * @param userId 用户ID
 * @returns
 */
export async function getUserRoles(userId: number | string) {
  return await request<Data<number> | Data<string>>(CONTEXT_PATH + 'user-roles', {
    method: 'GET',
    params: {
      userId,
    },
  });
}

/**
 * 为角色分配菜单
 *
 * @param roleIds   角色列表
 * @param menuIds   菜单列表
 * @returns
 */
export async function grantRoles(userId: number | string, roleIds: number[] | string[]) {
  return request(CONTEXT_PATH + 'grant-roles', {
    method: 'POST',
    params: {
      userId,
      roleIds,
    },
  });
}
