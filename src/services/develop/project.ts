// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { ProjectItem, ProjectParams } from '@/services/develop/types';
import { Data } from '@/services/types';

/**
 * API根路径
 */
const CONTEXT_PATH = '/api/szhome-develop/develop/project/';

/**
 * 分页查询工程列表
 *
 * @param params 工程分页查询条件
 * @returns 返回工程列表
 */
export async function pageProject(params: ProjectParams) {
  return request<Data<ProjectItem>>(CONTEXT_PATH + 'page', {
    method: 'GET',
    params,
  });
}

/**
 * 查询工程列表
 *
 * @param params 工程查询条件
 * @returns 返回工程列表
 */
export async function listProject(params: ProjectParams) {
  return request<Data<ProjectItem>>(CONTEXT_PATH + 'list', {
    method: 'GET',
    params,
  });
}

/** 新建工程 */
export async function addProject(data: ProjectItem) {
  return request<ProjectItem>(CONTEXT_PATH + 'add', {
    method: 'POST',
    data,
  });
}

/**
 * 更新工程信息
 *
 * @param data 工程信息
 * @returns
 */
export async function updateProject(data: ProjectItem) {
  return request<ProjectItem>(CONTEXT_PATH + 'update', {
    method: 'POST',
    data,
  });
}

/**
 * 删除工程
 *
 * @param ids 工程ID
 * @returns
 */
export async function removeProject(ids: Array<number | string>) {
  return request<Record<string, any>>(CONTEXT_PATH + 'delete', {
    method: 'POST',
    data: ids,
  });
}
