// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { TemplateSchemaItem, TemplateSchemaParams } from '@/services/develop/types';
import { Data } from '@/services/types';

/**
 * API根路径
 */
const CONTEXT_PATH = '/api/szhome-develop/develop/templateSchema/';

/**
 * 分页查询模板方案列表
 *
 * @param params 模板方案分页查询条件
 * @returns 返回模板方案列表
 */
export async function pageTemplateSchema(params: TemplateSchemaParams) {
  return request<Data<TemplateSchemaItem>>(CONTEXT_PATH + 'page', {
    method: 'GET',
    params,
  });
}

/**
 * 查询模板方案列表
 *
 * @param params 模板方案查询条件
 * @returns 返回模板方案列表
 */
export async function listTemplateSchema(params: TemplateSchemaParams) {
  return request<Data<TemplateSchemaItem>>(CONTEXT_PATH + 'list', {
    method: 'GET',
    params,
  });
}

/** 新建模板方案 */
export async function addTemplateSchema(data: TemplateSchemaItem) {
  return request<TemplateSchemaItem>(CONTEXT_PATH + 'add', {
    method: 'POST',
    data,
  });
}

/**
 * 更新模板方案信息
 *
 * @param data 模板方案信息
 * @returns
 */
export async function updateTemplateSchema(data: TemplateSchemaItem) {
  return request<TemplateSchemaItem>(CONTEXT_PATH + 'update', {
    method: 'POST',
    data,
  });
}

/**
 * 删除模板方案
 *
 * @param ids 模板方案ID
 * @returns
 */
export async function removeTemplateSchema(ids: Array<number | string>) {
  return request<Record<string, any>>(CONTEXT_PATH + 'delete', {
    method: 'POST',
    data: ids,
  });
}
