// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { PropertyItem, PropertyParams } from '@/services/develop/types';
import { Data } from '@/services/types';

/**
 * API根路径
 */
const CONTEXT_PATH = '/api/szhome-develop/develop/property/';

/**
 * 分页查询属性列表
 *
 * @param params 属性分页查询条件
 * @returns 返回属性列表
 */
export async function pageProperty(params: PropertyParams) {
  return request<Data<PropertyItem>>(CONTEXT_PATH + 'page', {
    method: 'GET',
    params,
  });
}

/**
 * 查询属性列表
 *
 * @param params 属性查询条件
 * @returns 返回属性列表
 */
export async function listProperty(params: PropertyParams) {
  return request<Data<PropertyItem>>(CONTEXT_PATH + 'list', {
    method: 'GET',
    params,
  });
}

/** 新建属性 */
export async function addProperty(data: PropertyItem) {
  return request<PropertyItem>(CONTEXT_PATH + 'add', {
    method: 'POST',
    data,
  });
}

/**
 * 更新属性信息
 *
 * @param data 属性信息
 * @returns
 */
export async function updateProperty(data: PropertyItem) {
  return request<PropertyItem>(CONTEXT_PATH + 'update', {
    method: 'POST',
    data,
  });
}

/**
 * 删除属性
 *
 * @param ids 属性ID
 * @returns
 */
export async function removeProperty(ids: Array<number | string>) {
  return request<Record<string, any>>(CONTEXT_PATH + 'delete', {
    method: 'POST',
    data: ids,
  });
}

/**
 * 删除属性
 *
 * @param ids 属性ID
 * @returns
 */
export async function settingProperty(settings: { modelId: string; properties: PropertyItem[] }) {
  return request<Record<string, any>>(CONTEXT_PATH + 'setting', {
    method: 'POST',
    data: settings,
  });
}
