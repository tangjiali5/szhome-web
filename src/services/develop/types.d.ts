/***** 数据源 *****/
export type DatasourceItem = {
  id: string;
  name?: string;
  url: string;
  username: string;
  password: string;
  tablePrefix: string;
  type: string;
  remark: string;
  createUser: number;
  createTime: Date;
  updateUser: number;
  updateTime: Date;
};

export type DatasourceParams = {
  status?: string;
  name?: string;
  desc?: string;
  key?: number;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};

/***** 模型 *****/
export type ModelItem = {
  id: string;
  datasourceid: string;
  projectId: string;
  tableName: string;
  tablePrefix: string;
  comments: string;
  code: string;
  name: string;
  module: string;
  author: string;
  tree: boolean;
  page: boolean;
  layout: string;
  remark: string;
  createUser: string;
  createTime: date;
  updateUser: string;
  updateTime: date;
};

export type ModelParams = {
  id: string;
  datasourceid: string;
  projectId: string;
  tableName: string;
  tablePrefix: string;
  comments: string;
  code: string;
  name: string;
  module: string;
  author: string;
  tree: boolean;
  page: boolean;
  layout: string;
  remark: string;
  createUser: string;
  createTime: date;
  updateUser: string;
  updateTime: date;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};

/***** 工程 *****/
export type ProjectItem = {
  id: string;
  name: string;
  code: string;
  basePkg: string;
  templateSchemaId: string;
  remark: string;
  createUser: string;
  createTime: Date;
  updateUser: string;
  updateTime: Date;
  deleteFlag: number;
};

export type ProjectParams = {
  id: string;
  name: string;
  code: string;
  basePkg: string;
  templateSchemaId: string;
  remark: string;
  createUser: string;
  createTime: Date;
  updateUser: string;
  updateTime: Date;
  deleteFlag: number;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};

/***** 属性 *****/
export type PropertyItem = {
  id: string;
  datasourceId: string;
  modelId: string;
  columnName: string;
  columnType: string;
  columnNullable: boolean;
  columnLength: number;
  columnScale: number;
  columnDefaultValue: string;
  columnComment: string;
  fieldName: string;
  fieldLabel: string;
  fieldType: string;
  fieldDict: string;
  fieldExample: string;
  fieldRemark: string;
  jsType: string;
  jsComponent: string;
  formSpan: number;
  formSearchShow: boolean;
  formSearchMatch: string;
  formSearchSort: number;
  formEditSort: number;
  formAddShow: boolean;
  formEditShow: boolean;
  formEditReadonly: boolean;
  tableShow: boolean;
  tableAlign: string;
  tableWidth: string;
  fixed: string;
  tableEllipsis: boolean;
  tableCopyable: boolean;
  validateType: string | null;
  validateValue: number[];
  validateValueMin: number;
  validateValueMax: number;
  validateLength: number[];
  validateLengthMin: number;
  validateLengthMax: number;
  validateRegex: string;
  sort: number;
  createUser: string;
  createTime: date;
  updateUser: string;
  updateTime: date;
};

export type PropertyParams = {
  id?: string;
  datasourceId?: string;
  modelId: string;
  columnName?: string;
  columnType?: string;
  columnNullable?: boolean;
  columnLength?: number;
  columnScale?: number;
  columnDefaultValue?: string;
  columnComment?: string;
  fieldName?: string;
  fieldLabel?: string;
  fieldType?: string;
  fieldDict?: string;
  fieldExample?: string;
  fieldRemark?: string;
  jsType?: string;
  jsComponent?: string;
  formSpan?: number;
  formSearchShow?: boolean;
  formSearchMatch?: string;
  formSearchSort?: number;
  formEditSort: number;
  formAddShow?: boolean;
  formEditShow?: boolean;
  formEditReadonly?: boolean;
  tableShow?: boolean;
  tableAlign?: string;
  tableWidth?: string;
  fixed: string;
  tableEllipsis?: boolean;
  tableCopyable?: boolean;
  validateType?: string;
  validateValueMin?: number;
  validateValueMax?: number;
  validateLengthMin?: number;
  validateLengthMax?: number;
  validateRegex?: string;
  sort?: number;
  createUser?: string;
  createTime?: date;
  updateUser?: string;
  updateTime?: date;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};

/***** 模版 *****/
export type TemplateItem = {
  id: string;
  templateSchemaid: string;
  name: string;
  code: string;
  type: string;
  rootPath: string;
  parentPath: string;
  file: string;
  fileType: string;
  template?: string;
  engine: string;
  remark: string;
  createUser: string;
  createTime: Date;
  updateUser: string;
  updateTime: Date;
  deleteFlag: number;
};

export type TemplateParams = {
  id: string;
  templateSchemaid: string;
  name: string;
  code: string;
  type: string;
  rootPath: string;
  parentPath: string;
  file: string;
  fileType: string;
  template: string;
  engine: string;
  remark: string;
  createUser: string;
  createTime: Date;
  updateUser: string;
  updateTime: Date;
  deleteFlag: number;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};

/***** 模板方案 *****/
export type TemplateSchemaItem = {
  id: string;
  name: string;
  code: string;
  remark: string;
  createUser: string;
  createTime: Date;
  updateUser: string;
  updateTime: Date;
  deleteFlag: number;
};

export type TemplateSchemaParams = {
  id: string;
  name: string;
  code: string;
  remark: string;
  createUser: string;
  createTime: Date;
  updateUser: string;
  updateTime: Date;
  deleteFlag: number;
  pageSize?: number;
  currentPage?: number;
  filter?: Record<string, any[]>;
  sorter?: Record<string, any>;
};
