// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { DatasourceItem, DatasourceParams } from '@/services/develop/types';
import { Data } from '@/services/types';

/**
 * API根路径
 */
const CONTEXT_PATH = '/api/szhome-develop/develop/datasource/';

/**
 * 分页查询数据源列表
 *
 * @param params 数据源分页查询条件
 * @returns 返回数据源列表
 */
export async function pageDatasource(params: DatasourceParams) {
  return request<Data<DatasourceItem>>(CONTEXT_PATH + 'page', {
    method: 'GET',
    params,
  });
}

/**
 * 查询数据源列表
 *
 * @param params 数据源查询条件
 * @returns 返回数据源列表
 */
export async function listDatasource(params: DatasourceParams) {
  return request<Data<DatasourceItem>>(CONTEXT_PATH + 'list', {
    method: 'GET',
    params,
  });
}

/** 新建数据源 */
export async function addDatasource(data: DatasourceItem) {
  return request<DatasourceItem>(CONTEXT_PATH + 'add', {
    method: 'POST',
    data,
  });
}

/**
 * 更新数据源信息
 *
 * @param data 数据源信息
 * @returns
 */
export async function updateDatasource(data: DatasourceItem) {
  return request<DatasourceItem>(CONTEXT_PATH + 'update', {
    method: 'POST',
    data,
  });
}

/**
 * 删除数据源
 *
 * @param ids 数据源ID
 * @returns
 */
export async function removeDatasource(ids: Array<number | string>) {
  return request<Record<string, any>>(CONTEXT_PATH + 'delete', {
    method: 'POST',
    data: ids,
  });
}

/**
 * 同步数据源
 *
 * @param ids 数据源ID
 * @returns
 */
export async function syncDatasource(ids: Array<number | string>) {
  return request<Record<string, any>>(CONTEXT_PATH + 'sync', {
    method: 'POST',
    data: ids,
  });
}
