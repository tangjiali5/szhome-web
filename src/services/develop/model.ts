// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { ModelItem, ModelParams } from '@/services/develop/types';
import { Data } from '@/services/types';

/**
 * API根路径
 */
const CONTEXT_PATH = '/api/szhome-develop/develop/model/';

/**
 * 分页查询模型列表
 *
 * @param params 模型分页查询条件
 * @returns 返回模型列表
 */
export async function pageModel(params: ModelParams) {
  return request<Data<ModelItem>>(CONTEXT_PATH + 'page', {
    method: 'GET',
    params,
  });
}

/** 新建模型 */
export async function addModel(data: ModelItem) {
  return request<ModelItem>(CONTEXT_PATH + 'add', {
    method: 'POST',
    data,
  });
}

/**
 * 更新模型信息
 *
 * @param data 模型信息
 * @returns
 */
export async function updateModel(data: ModelItem) {
  return request<ModelItem>(CONTEXT_PATH + 'update', {
    method: 'POST',
    data,
  });
}

/**
 * 删除模型
 *
 * @param ids 模型ID
 * @returns
 */
export async function removeModel(ids: Array<number | string>) {
  return request<Record<string, any>>(CONTEXT_PATH + 'delete', {
    method: 'POST',
    data: ids.join(','),
  });
}
