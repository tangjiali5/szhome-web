// @ts-ignore
/* eslint-disable */
import { request } from 'umi';
import { TemplateItem, TemplateParams } from '@/services/develop/types';
import { Data } from '@/services/types';

/**
 * API根路径
 */
const CONTEXT_PATH = '/api/szhome-develop/develop/template/';

/**
 * 分页查询模版列表
 *
 * @param params 模版分页查询条件
 * @returns 返回模版列表
 */
export async function pageTemplate(params: TemplateParams) {
  return request<Data<TemplateItem>>(CONTEXT_PATH + 'page', {
    method: 'GET',
    params,
  });
}

/**
 * 新建模板
 * @param data 模板数据
 * @returns
 */
export async function addTemplate(data: TemplateItem) {
  return request<TemplateItem>(CONTEXT_PATH + 'add', {
    method: 'POST',
    data,
  });
}

/**
 * 更新模版信息
 *
 * @param data 模版信息
 * @returns
 */
export async function updateTemplate(data: TemplateItem) {
  return request<TemplateItem>(CONTEXT_PATH + 'update', {
    method: 'POST',
    data,
  });
}

/**
 * 删除模版
 *
 * @param ids 模版ID
 * @returns
 */
export async function removeTemplate(ids: Array<number | string>) {
  return request<Record<string, any>>(CONTEXT_PATH + 'delete', {
    method: 'POST',
    data: ids,
  });
}

/**
 * 模板列表
 *
 * @param params
 * @returns
 */
export async function listTemplate(params: {}) {
  return request<Data<TemplateItem>>(CONTEXT_PATH + 'list', {
    method: 'GET',
    params,
  });
}
