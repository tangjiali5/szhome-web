// @ts-ignore
/* eslint-disable */
import { request } from 'umi';

/**
 * API根路径
 */
const CONTEXT_PATH = '/api/szhome-develop/develop/generator/';

/**
 * 生成模型常用脚本
 *
 * @param params 数据源分页查询条件
 * @returns 返回数据源列表
 */
export async function generateScript(params: {
  modelId: string;
  templateSchemaCode?: string;
  templateCode: string;
}) {
  return await request<{ data: string }>(CONTEXT_PATH + 'script', {
    method: 'GET',
    params: {
      templateSchemaCode: 'system',
      ...params,
    },
  });
}

/**
 * 预览模型生成代码
 *
 * @param modelId 模型ID
 * @returns
 */
export async function preview(modelId: string | string[]) {
  return await request<{ data: any }>(CONTEXT_PATH + 'preview/' + modelId, {
    method: 'GET',
  });
}
