import { ProFormText } from '@ant-design/pro-form';

const valueTypes = {
  link: {
    render: (text) => <a>{text}</a>,
    renderFormItem: (text, props) => <ProFormText {...props?.fieldProps} />,
  },
};

export default valueTypes;
