import { treeMenu } from '@/services/system/menu';
import { ProFormTreeSelect } from '@ant-design/pro-form';
import React from 'react';

type MenuTreeProps = {
  title: string;
  name: string;
  cols?: number;
  required?: boolean;
};

/**
 * 菜单树
 * @param props
 * @returns
 */
const MenuTree: React.FC<MenuTreeProps> = (props) => {
  return (
    <ProFormTreeSelect
      label={props.title}
      name={props.name}
      colProps={{ span: props.cols || 24 }}
      required={props.required || false}
      request={async () => {
        const res = await treeMenu({});
        return res.data;
      }}
      fieldProps={{
        treeDefaultExpandAll: true,
        treeLine: true,
        treeIcon: true,
        fieldNames: {
          label: 'name',
          value: 'id',
        },
      }}
    />
  );
};

export default MenuTree;
