import { listUser } from '@/services/system/user';
import { ProFormSelect } from '@ant-design/pro-form';
import React from 'react';

type UserSelectProps = {
  title: string;
  name: string;
  cols?: number;
  required?: boolean;
};

/**
 * 用户选择器
 * @param props
 * @returns
 */
const UserSelect: React.FC<UserSelectProps> = (props) => {
  return (
    <ProFormSelect
      label={props.title}
      name={props.name}
      colProps={{ span: props.cols || 24 }}
      required={props.required || false}
      request={listUser}
    />
  );
};

export default UserSelect;
