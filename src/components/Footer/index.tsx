import { useIntl } from 'umi';
import { AntDesignOutlined, GithubOutlined, MailOutlined } from '@ant-design/icons';
import { DefaultFooter } from '@ant-design/pro-layout';

const Footer: React.FC = () => {
  const intl = useIntl();
  const defaultMessage = intl.formatMessage({
    id: 'app.copyright.produced',
    defaultMessage: '玉诚开发工具-齐简',
  });

  const currentYear = new Date().getFullYear();

  return (
    <DefaultFooter
      copyright={`${currentYear} ${defaultMessage}`}
      links={[
        {
          key: 'szhome',
          title: (
            <>
              <MailOutlined />
              &nbsp;齐简
            </>
          ),
          href: 'mailto://tangjiali@inspur.com',
          blankTarget: true,
        },
        {
          key: 'github',
          title: (
            <>
              <GithubOutlined />
              &nbsp;Github
            </>
          ),
          href: 'https://github.com/tangjiali',
          blankTarget: true,
        },
        {
          key: 'antd',
          title: (
            <>
              <AntDesignOutlined />
              &nbsp;Ant Design
            </>
          ),
          href: 'https://ant.design',
          blankTarget: true,
        },
      ]}
    />
  );
};

export default Footer;
