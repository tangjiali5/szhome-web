import { getDictValues } from '@/services/system/dictValue';
import { ProFormSelect } from '@ant-design/pro-form';
import type { ProFormSelectProps } from '@ant-design/pro-form/lib/components/Select';
import React from 'react';

type DictSelectProps = {
  label?: string;
  name?: string;
  dict: string;
  cols?: number;
} & Partial<ProFormSelectProps>;

/**
 * 数据字典下拉列表
 * @param props
 * @returns
 */
const DictSelect: React.FC<DictSelectProps> = (props) => {
  return (
    <ProFormSelect
      required
      label={props.label}
      name={props.name}
      request={() => getDictValues(props.dict, false)}
      colProps={{ span: props.cols }}
      {...props}
    />
  );
};

export default DictSelect;
