import { treeDept } from '@/services/system/dept';
import { ProFormTreeSelect } from '@ant-design/pro-form';
import React from 'react';

type DeptTreeProps = {
  title: string;
  name: string;
  cols?: number;
  required?: boolean;
};

/**
 * 组织架构树
 * @param props
 * @returns
 */
const DeptTree: React.FC<DeptTreeProps> = (props) => {
  return (
    <ProFormTreeSelect
      label={props.title}
      name={props.name}
      colProps={{ span: props.cols || 24 }}
      required={props.required || false}
      request={async () => {
        const res = await treeDept({});
        return res.data;
      }}
      fieldProps={{
        treeDefaultExpandAll: true,
        treeLine: true,
        treeIcon: true,
        fieldNames: {
          label: 'name',
          value: 'id',
        },
      }}
    />
  );
};

export default DeptTree;
