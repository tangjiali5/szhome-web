import React from 'react';
import type { ProTableProps } from '@ant-design/pro-table';
import ProTable from '@ant-design/pro-table';

/**
 * 定义表格配置属性
 */
type SzhomeTableProps<T, U> = {} & ProTableProps<T, U>;

/**
 * 实现自定义表格
 */
const SzhomeTable = React.forwardRef((props: SzhomeTableProps<any, any>, ref) => {
  return (
    <ProTable
      rowKey="id"
      bordered
      search={{
        labelWidth: 80,
      }}
      pagination={
        props.pagination === false
          ? false
          : {
              pageSize: props.pagination?.pageSize ? props.pagination?.pageSize : 10,
              // pageSizeOptions: [10, 20, 50, 100],
              // showSizeChanger: true,
            }
      }
      expandable={{
        indentSize: 40,
        defaultExpandAllRows: true,
      }}
      options={{
        fullScreen: true,
      }}
      {...props}
    />
  );
}) as <T, U>(props: SzhomeTableProps<T, U>) => JSX.Element;

export default SzhomeTable;
