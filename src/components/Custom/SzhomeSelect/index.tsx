import React from 'react';
import type { ProFormSelectProps } from '@ant-design/pro-form/lib/components/Select';
import { ProFormSelect } from '@ant-design/pro-form';

/**
 * 定义表格配置属性
 */
type SzhomeSelectProps = {
  fn: (params: any) => Promise<any>;
} & Omit<ProFormSelectProps, 'request'>;

/**
 * 实现自定义表格
 */
const SzhomeSelect = React.forwardRef((props: SzhomeSelectProps) => {
  return (
    <ProFormSelect
      fieldProps={{
        fieldNames: {
          label: 'name',
          value: 'id',
        },
      }}
      request={() => {
        return new Promise(async (resolve) => {
          const res = await props.fn(props.params);
          resolve(res.data);
        });
      }}
      {...props}
    />
  );
}) as (props: SzhomeSelectProps) => JSX.Element;

export default SzhomeSelect;
